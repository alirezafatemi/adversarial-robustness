Finetune natural model with 10 or 1 percent of data:
```shell
!python -m src.main_finetune \
    --run_id "finetune10percent" \
    --seed 42 \
    --dataset "cifar-10" \
    --model "resnet18" \
    --optimizer "sgd" \
    --percentage_labeled_data 10 \
    --use_validation \
    --lr 0.1 \
    --weight_decay 2e-4 \
    --lr_scheduler "multi_step2" \
    --batch_size 128 \
    --encoder_trainable \
    --epochs 100 \
    --save_frequency 30 \
    --base_dir "/content/drive/MyDrive/Robustness/self-supervised/natural-byol/100" \
    --pretrain_checkpoint_path "/content/drive/MyDrive/Robustness/self-supervised/natural-byol/100/pretrain/models/checkpoint.pt" \
    --image_size 32 \
    --use_random_crop
```
Extract pseudo labels:
```shell
!python -W ignore -m src.main_pseudo_label \
    --checkpoint_path "/content/drive/My Drive/Robustness/self-supervised/natural-byol/100/finetune10percent/models/best_checkpoint.pt" \
    --output_path "/content/drive/My Drive/Robustness/self-supervised/natural-byol/100/finetune10percent/pseudo_labels.npy" \
    --image_size 32 \
    --model "resnet18" \
    --batch_size 1024
```
Semi-supervised training:
```shell
!python -m src.main_semi_supervised \
    --run_id "semi10percent" \
    --seed 42 \
    --dataset "cifar-10" \
    --model "resnet18" \
    --optimizer "sgd" \
    --percentage_labeled_data 10 \
    --lr 0.1 \
    --weight_decay 2e-4 \
    --lr_scheduler "multi_step3" \
    --batch_size 128 \
    --epochs 15 \
    --save_frequency 5 \
    --base_dir "/content/drive/MyDrive/Robustness/self-supervised/adv-swav/100" \
    --pretrain_checkpoint_path "/content/drive/MyDrive/Robustness/self-supervised/adv-byol/100/pretrain/models/checkpoint.pt" \
    --pseudo_labels_path "/content/drive/My Drive/Robustness/self-supervised/natural-byol/100/finetune10percent/pseudo_labels.npy" \
    --alpha_coefficient 0.5 \
    --beta 6.0 \
    --rate_distill 1.0 \
    --temperature 1.0 \
    --image_size 32 \
    --use_random_crop \
    --attack_iters 10 \
    --random_start True
```