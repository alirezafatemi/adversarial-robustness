Natural SimCLR Run:
```shell script
!python -W ignore -m src.main_pretrain \
    --run_id "pretrain" \
    --method "simclr" \
    --seed 42 \
    --resume \
    --checkpoint_path "/content/drive/My Drive/Robustness/self-supervised/natural-simclr/1/pretrain/models/checkpoint.pt" \
    --projection_size 64 \
    --projection_hidden_size 1024 \
    --num_projection_layers 2 \
    --temperature 0.5 \
    --dataset "cifar-10" \
    --model "resnet18" \
    --optimizer "lars" \
    --lr 0.12 \
    --weight_decay 1e-6 \
    --lr_scheduler "cosine" \
    --use_warmup \
    --lr_multiplier 10.0 \
    --warmup_steps 10 \
    --batch_size 1024 \
    --save_frequency 10 \
    --epochs 1000 \
    --base_dir "/content/drive/My Drive/Robustness/self-supervised/natural-simclr/1" \
    --knn_statistics \
    --image_size 32 \
    --use_color_jitter \
    --color_jitter_strength 0.5 \
    --use_grayscale \
    --grayscale_prob 0.2 \
    --use_random_resized_crop \
    --min_scale_crops 0.2 \
    --max_scale_crops 1.0 \
    --log_images
```
Natural BYOL:
```shell
!python -W ignore -m src.main_pretrain \
    --run_id "pretrain" \
    --method "byol" \
    --seed 42 \
    --resume \
    --checkpoint_path "/content/drive/My Drive/Robustness/self-supervised/natural-byol/1/pretrain/models/checkpoint.pt" \
    --projection_size 256 \
    --projection_hidden_size 4096 \
    --num_projection_layers 2 \
    --projector_hidden_bn \
    --prediction_hidden_size 4096 \
    --num_prediction_layers 2 \
    --predictor_hidden_bn \
    --moving_average_decay 0.996 \
    --dataset "cifar-10" \
    --model "resnet18" \
    --optimizer "lars" \
    --lr 0.12 \
    --min_lr 0.00012 \
    --weight_decay 1e-6 \
    --lr_scheduler "cosine" \
    --use_warmup \
    --lr_multiplier 10.0 \
    --warmup_steps 10 \
    --batch_size 1024 \
    --save_frequency 10 \
    --epochs 1000 \
    --base_dir "/content/drive/My Drive/Robustness/self-supervised/natural-byol/1" \
    --knn_statistics \
    --image_size 32 \
    --use_color_jitter \
    --color_jitter_strength 0.5 \
    --use_grayscale \
    --grayscale_prob 0.2 \
    --use_random_resized_crop \
    --min_scale_crops 0.2 \
    --max_scale_crops 1.0 \
    --log_images
```
Natural SwAV:
```shell
!python -W ignore -m src.main_pretrain \
    --run_id "pretrain" \
    --method "swav" \
    --seed 42 \
    --resume \
    --checkpoint_path "/content/drive/My Drive/Robustness/self-supervised/natural-swav/2/pretrain/models/checkpoint.pt" \
    --projection_size 128 \
    --projection_hidden_size 2048 \
    --num_projection_layers 2 \
    --projector_hidden_bn \
    --dataset "cifar-10" \
    --model "resnet18" \
    --optimizer "lars" \
    --lr 0.12 \
    --min_lr 0.0012 \
    --weight_decay 1e-6 \
    --lr_scheduler "cosine" \
    --use_warmup \
    --lr_multiplier 10.0 \
    --warmup_steps 10 \
    --batch_size 1024 \
    --save_frequency 50 \
    --epochs 800 \
    --base_dir "/content/drive/My Drive/Robustness/self-supervised/natural-swav/2" \
    --knn_statistics \
    --image_size 32 \
    --use_color_jitter \
    --color_jitter_strength 0.5 \
    --use_grayscale \
    --grayscale_prob 0.2 \
    --use_random_resized_crop \
    --min_scale_crops 0.08 \
    --max_scale_crops 1.0 \
    --num_prototypes 100 \
    --sinkhorn_iterations 3 \
    --swav_epsilon 0.05 \
    --freeze_prototypes_num_iters 50 \
    --temperature 0.1 \
    --log_images
```
Perceptual SimCLR:
```shell
!python -m src.main_pretrain \
    --run_id "pretrain" \
    --method "simclr" \
    --seed 42 \
    --resume \
    --checkpoint_path "/content/drive/My Drive/Robustness/self-supervised/perceptual-simclr/1/pretrain/models/checkpoint.pt" \
    --projection_size 64 \
    --projection_hidden_size 1024 \
    --num_projection_layers 2 \
    --temperature 0.5 \
    --dataset "cifar-10" \
    --model "resnet18" \
    --optimizer "lars" \
    --lr 0.12 \
    --weight_decay 1e-6 \
    --lr_scheduler "cosine" \
    --use_warmup \
    --lr_multiplier 10.0 \
    --warmup_steps 10 \
    --batch_size 1024 \
    --save_frequency 10 \
    --epochs 1000 \
    --base_dir "/content/drive/My Drive/Robustness/self-supervised/perceptual-simclr/1" \
    --knn_statistics \
    --image_size 32 \
    --adv_training \
    --perceptual_adv_training \
    --attack_iters 10 \
    --bound 0.25 \
    --lam 1.0 \
    --log_images
```

https://github.com/jramapuram/BYOL/blob/master/main.py
https://github.com/sthalles/PyTorch-BYOL/
https://sslwin.org/assets/sslwin_cd.pdf
https://github.com/Spijkervet/BYOL/
https://github.com/deepmind/deepmind-research/tree/master/byol


Models:
https://github.com/pytorch/vision/blob/master/torchvision/models/resnet.py
https://github.com/osmr/imgclsmob/blob/master/pytorch/README.md
https://github.com/osmr/imgclsmob/blob/master/pytorch/pytorchcv/models/preresnet.py
https://github.com/pytorch/vision/blob/master/torchvision/models/resnet.py


Great sources for Adversarial:
https://deepai.org/publication/bag-of-tricks-for-adversarial-training
https://github.com/kuangliu/pytorch-cifar
https://github.com/P2333/Bag-of-Tricks-for-AT
https://pytorch.org/docs/master/generated/torch.nn.BatchNorm2d.html#torch.nn.BatchNorm2d
https://github.com/Trusted-AI/adversarial-robustness-toolbox/wiki/ART-Attacks#1-evasion-attacks


Maybe later:
https://www.youtube.com/watch?v=5qefnAek8OA
https://www.youtube.com/watch?v=nUUqwaxLnWs
https://savan77.github.io/blog/imagenet_adv_examples.html
