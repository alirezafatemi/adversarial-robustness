from src.arguments import init_finetune_argparse
from src.configuration import (
    AugmentationConfiguration,
    DirectoryConfiguration,
    FinetuneAttackConfiguration,
    FinetuneConfiguration,
    LRSchedulerConfiguration,
    OptimizerConfiguration,
)
from src.initialize import prepare_cudnn, set_seed
from src.methods.finetune import Finetune


def main():
    parser = init_finetune_argparse()
    args = parser.parse_args()
    # Prepare Environment
    set_seed(seed=args.seed)
    prepare_cudnn(deterministic=True, benchmark=True)

    directory_configuration = DirectoryConfiguration(
        base_dir=args.base_dir,
        checkpoint_path=args.checkpoint_path,
        pretrain_checkpoint_path=args.pretrain_checkpoint_path,
    )

    excluded_parameters = []
    if args.exclude_bias_parameters_from_lars:
        excluded_parameters.append(".bias")
    if args.exclude_bn_parameters_from_lars:
        excluded_parameters.append(".bn")
    optimizer_configuration = OptimizerConfiguration(
        optimizer=args.optimizer,
        weight_decay=args.weight_decay,
        lr=args.lr,
        momentum=args.momentum,
        nesterov=args.nesterov,
        excluded_parameters=excluded_parameters,
    )

    lr_scheduler_configuration = LRSchedulerConfiguration(
        lr_scheduler=args.lr_scheduler,
        use_warmup=args.use_warmup,
        lr_multiplier=args.lr_multiplier,
        warmup_steps=args.warmup_steps,
        min_lr=args.min_lr,
    )

    augmentation_configuration = AugmentationConfiguration(
        dataset=args.dataset,
        image_size=args.image_size,
        use_random_resized_crop=args.use_random_resized_crop,
        use_random_crop=args.use_random_crop,
        use_resize=args.use_resize,
        use_gaussian_blur=args.use_gaussian_blur,
        normalize=args.normalize,
        use_color_jitter=args.use_color_jitter,
        use_grayscale=args.use_grayscale,
        min_scale_crops=args.min_scale_crops,
        max_scale_crops=args.max_scale_crops,
        color_jitter_strength=args.color_jitter_strength,
        grayscale_prob=args.grayscale_prob,
    )

    adv_configuration = FinetuneAttackConfiguration(
        attack_norm=args.attack_norm,
        random_start=args.random_start,
        iters=args.attack_iters,
        epsilon=args.epsilon,
        alpha=args.alpha,
        min_val=args.min_val,
        max_val=args.max_val,
        auto_attack=args.auto_attack,
    )

    configuration = FinetuneConfiguration(
        seed=args.seed,
        use_dual_bn=args.use_dual_bn,
        batch_size=args.batch_size,
        model=args.model,
        epochs=args.epochs,
        dataset_path=args.dataset_path,
        resume=args.resume,
        run_id=args.run_id,
        save_frequency=args.save_frequency,
        directories=directory_configuration,
        optimizer_cfg=optimizer_configuration,
        lr_scheduler_cfg=lr_scheduler_configuration,
        augmentation_cfg=augmentation_configuration,
        adv_training=args.adv_training,
        adv_statistics=args.adv_statistics,
        adv_cfg=adv_configuration,
        encoder_trainable=args.encoder_trainable,
        pretrain_checkpoint_path=args.pretrain_checkpoint_path,
        percentage_labeled_data=args.percentage_labeled_data,
        use_validation=args.use_validation,
        cifar_stem=True,
        log_images=args.log_images,
        use_amp=args.use_amp,
        use_frozen_features=args.use_frozen_features,
        use_natural_bn=args.use_natural_bn,
        trades=args.trades,
        beta=args.beta,
        # TODO: Not related to finetune
        visualize=args.visualize,
        visualization_dimension=args.visualization_dimension,
        tsne_fig_size=args.tsne_fig_size,
    )

    method = Finetune(cfg=configuration)
    if args.visualize:
        # TODO: Not related to finetune
        method.visualize()
    else:
        method.finetune()


if __name__ == "__main__":
    main()
