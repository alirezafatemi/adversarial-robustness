from typing import Tuple

import numpy as np
from PIL import Image
from torch import Tensor
from torch.utils.data import ConcatDataset
from torchvision.datasets import SVHN
from torchvision.transforms import transforms

from src.augmentations import create_transforms
from src.configuration import AugmentationConfiguration


class ConcatSVHNDataset(ConcatDataset):
    def __init__(self, root: str, download: bool, transform=None):
        datasets = [
            SVHN(root=root, split="train", download=download, transform=transform),
            SVHN(root=root, split="extra", download=download, transform=transform),
        ]
        super().__init__(datasets)


class SVHNPair(SVHN):
    def __getitem__(self, idx: int) -> Tuple[Tensor, Tensor]:
        img, target = self.data[idx], int(self.labels[idx])
        img = Image.fromarray(np.transpose(img, (1, 2, 0)))
        return self.transform(img), self.transform(img)


def svhn_basic_transforms(
    augmentation_cfg: AugmentationConfiguration,
) -> transforms.Compose:
    return create_transforms(
        AugmentationConfiguration(
            dataset="svhn",
            image_size=32,
            use_resize=True,
            normalize=augmentation_cfg.normalize,
            use_random_horizontal_flip=False,
            use_random_resized_crop=False,
            use_random_crop=False,
            use_gaussian_blur=False,
            use_color_jitter=False,
            use_grayscale=False,
        )
    )


def svhn_target_transform(target: int) -> int:
    return target - 1


def get_svhn_datasets(
    pretrain_transform: transforms.Compose,
    train_transform: transforms.Compose,
    test_transform: transforms.Compose,
    root: str = "./data",
) -> Tuple[SVHNPair, SVHN, SVHN]:
    pretrain_dataset = SVHNPair(
        root=root,
        split="train",
        transform=pretrain_transform,
        download=True,
    )
    train_dataset = SVHN(
        root=root,
        split="train",
        transform=train_transform,
        target_transform=svhn_target_transform,
        download=True,
    )
    test_dataset = SVHN(
        root=root,
        split="test",
        transform=test_transform,
        target_transform=svhn_target_transform,
        download=True,
    )
    return pretrain_dataset, train_dataset, test_dataset
