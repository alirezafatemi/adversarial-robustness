from typing import Tuple

import torch
from PIL import Image
from torch import Tensor
from torchvision import transforms
from torchvision.datasets import CIFAR10, CIFAR100

from src.augmentations import create_transforms
from src.configuration import AugmentationConfiguration


class CIFAR10PseudoLabeled(CIFAR10):
    def __init__(
        self,
        pseudo_labels,
        root,
        train=True,
        transform=None,
        target_transform=None,
        download=False,
    ):
        super(CIFAR10PseudoLabeled, self).__init__(
            root, train, transform, target_transform, download
        )
        self.pseudo_labels = pseudo_labels

    def __getitem__(self, idx: int) -> Tuple[Tensor, Tensor, Tensor]:
        img = self.data[idx]
        img = Image.fromarray(img)
        img = self.transform(img)
        pseudo_label = torch.FloatTensor(self.pseudo_labels[idx])
        real_label = self.targets[idx]
        return img, pseudo_label, real_label


class CIFAR10Pair(CIFAR10):
    def __getitem__(self, idx: int) -> Tuple[Tensor, Tensor]:
        img = self.data[idx]
        img = Image.fromarray(img)
        return self.transform(img), self.transform(img)


class CIFAR100Pair(CIFAR100):
    def __getitem__(self, idx: int) -> Tuple[Tensor, Tensor]:
        img = self.data[idx]
        img = Image.fromarray(img)
        return self.transform(img), self.transform(img)


def cifar_basic_transforms(
    augmentation_cfg: AugmentationConfiguration,
) -> transforms.Compose:
    return create_transforms(
        AugmentationConfiguration(
            dataset=augmentation_cfg.dataset,
            image_size=32,
            use_resize=True,
            normalize=augmentation_cfg.normalize,
            use_random_horizontal_flip=False,
            use_random_resized_crop=False,
            use_random_crop=False,
            use_gaussian_blur=False,
            use_color_jitter=False,
            use_grayscale=False,
        )
    )


def get_cifar10_datasets(
    pretrain_transform: transforms.Compose,
    train_transform: transforms.Compose,
    test_transform: transforms.Compose,
    root: str = "./data",
) -> Tuple[CIFAR10Pair, CIFAR10, CIFAR10]:
    pretrain_dataset = CIFAR10Pair(
        root=root,
        train=True,
        transform=pretrain_transform,
        download=True,
    )
    train_dataset = CIFAR10(
        root=root,
        train=True,
        transform=train_transform,
        download=True,
    )
    test_dataset = CIFAR10(
        root=root,
        train=False,
        transform=test_transform,
        download=True,
    )
    return pretrain_dataset, train_dataset, test_dataset


def get_cifar100_datasets(
    pretrain_transform: transforms.Compose,
    train_transform: transforms.Compose,
    test_transform: transforms.Compose,
    root: str = "./data",
) -> Tuple[CIFAR100Pair, CIFAR100, CIFAR100]:
    pretrain_dataset = CIFAR100Pair(
        root=root,
        train=True,
        transform=pretrain_transform,
        download=True,
    )
    train_dataset = CIFAR100(
        root=root,
        train=True,
        transform=train_transform,
        download=True,
    )
    test_dataset = CIFAR100(
        root=root,
        train=False,
        transform=test_transform,
        download=True,
    )
    return pretrain_dataset, train_dataset, test_dataset
