from typing import Tuple

import numpy as np
from PIL import Image
from torch import Tensor
from torchvision.datasets import STL10
from torchvision.transforms import transforms

from src.augmentations import create_transforms
from src.configuration import AugmentationConfiguration

# https://cs.stanford.edu/~acoates/stl10/

STL10_CROP_SIZE = 96


class STL10Pair(STL10):
    def __getitem__(self, idx: int) -> Tuple[Tensor, Tensor]:
        img, target = self.data[idx]
        img = Image.fromarray(np.transpose(img, (1, 2, 0)))
        return self.transform(img), self.transform(img)


def stl10_basic_transforms(
    augmentation_cfg: AugmentationConfiguration,
) -> transforms.Compose:
    return create_transforms(
        AugmentationConfiguration(
            dataset="stl10",
            image_size=STL10_CROP_SIZE,
            use_resize=True,
            normalize=augmentation_cfg.normalize,
            use_random_horizontal_flip=False,
            use_random_resized_crop=False,
            use_random_crop=False,
            use_gaussian_blur=False,
            use_color_jitter=False,
            use_grayscale=False,
        )
    )


def get_stl10_datasets(
    pretrain_transform: transforms.Compose,
    train_transform: transforms.Compose,
    test_transform: transforms.Compose,
    root: str = "./data",
) -> Tuple[STL10Pair, STL10, STL10]:
    pretrain_dataset = STL10Pair(
        root=root,
        split="train+unlabeled",
        transform=pretrain_transform,
        download=True,
    )
    train_dataset = STL10(
        root=root,
        split="train",
        transform=train_transform,
        download=True,
    )
    test_dataset = STL10(
        root=root,
        split="test",
        transform=test_transform,
        download=True,
    )
    return pretrain_dataset, train_dataset, test_dataset
