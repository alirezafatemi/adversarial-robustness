from .cifar import (
    CIFAR100Pair,
    CIFAR10Pair,
    cifar_basic_transforms,
    get_cifar100_datasets,
    get_cifar10_datasets,
)
from .stl10 import STL10Pair, get_stl10_datasets, stl10_basic_transforms
from .svhn import SVHNPair, get_svhn_datasets, svhn_basic_transforms
