import torch

from src.warmup_scheduler import GradualWarmupScheduler

if __name__ == "__main__":
    v = torch.zeros(10)
    epochs = 100
    warmup_steps = 10
    optim = torch.optim.SGD([v], lr=0.01)
    lr_scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(
        optim, epochs - warmup_steps, eta_min=0
    )
    scheduler = GradualWarmupScheduler(
        optim, multiplier=10, total_epoch=warmup_steps, after_scheduler=lr_scheduler
    )

    print("Start", scheduler.last_epoch)
    for epoch in range(0, 40):
        print(epoch, scheduler.get_last_lr())
        scheduler.step()
        print("Last epoch: ", end="")
        if epoch >= warmup_steps:
            print(lr_scheduler.last_epoch)
        else:
            print(scheduler.last_epoch)
