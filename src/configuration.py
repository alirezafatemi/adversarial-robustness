from dataclasses import dataclass
from typing import List, Optional, Tuple, Union


@dataclass
class AugmentationConfiguration:
    dataset: str = "cifar-10"
    image_size: int = 32
    use_random_horizontal_flip: bool = True
    use_random_resized_crop: bool = True
    use_random_crop: bool = False
    use_resize: bool = False
    use_gaussian_blur: bool = False
    normalize: bool = False
    use_color_jitter: bool = True
    use_grayscale: bool = True
    min_scale_crops: float = 0.2
    max_scale_crops: float = 1.0
    color_jitter_strength: float = 0.5
    grayscale_prob: float = 0.2


@dataclass
class OptimizerConfiguration:
    optimizer: str
    weight_decay: float
    lr: float
    momentum: Optional[float]
    nesterov: Optional[bool]
    excluded_parameters: List[str]


@dataclass
class LRSchedulerConfiguration:
    lr_scheduler: str
    use_warmup: bool
    lr_multiplier: Optional[float]
    warmup_steps: Optional[int]
    min_lr: Optional[float]


@dataclass
class MethodConfiguration:
    projection_size: int
    projection_hidden_size: int
    num_projection_layers: int
    projector_hidden_bn: bool
    projector_out_bn: bool


@dataclass
class BYOLConfiguration(MethodConfiguration):
    prediction_hidden_size: int
    num_prediction_layers: int
    predictor_hidden_bn: bool
    predictor_out_bn: bool
    moving_average_decay: float = 0.996
    # Preferably set moving_average_decay to 0.99 if use_cosine_increasing_schedule is True
    use_cosine_increasing_schedule: bool = False
    use_contrastive_loss_in_train: bool = False
    use_contrastive_loss_in_attack: bool = False


@dataclass
class BYOLCLRConfiguration(MethodConfiguration):
    moving_average_decay: float = 0.996
    # Preferably set moving_average_decay to 0.99 if use_cosine_increasing_schedule is True
    use_cosine_increasing_schedule: bool = False


@dataclass
class SimSiamConfiguration(MethodConfiguration):
    prediction_hidden_size: int
    num_prediction_layers: int
    predictor_hidden_bn: bool
    predictor_out_bn: bool
    detach_targets: bool = True
    use_contrastive_loss_in_train: bool = False
    use_contrastive_loss_in_attack: bool = False


@dataclass
class SwAVConfiguration(MethodConfiguration):
    freeze_prototypes_num_iters: int  # Freeze prototypes for one epoch
    num_prototypes: int = 100
    epsilon: float = 0.05
    sinkhorn_iterations: int = 3
    temperature: float = 0.1


@dataclass
class SimCLRConfiguration(MethodConfiguration):
    temperature: float = 0.5
    concat_inputs: bool = True


@dataclass
class DirectoryConfiguration:
    base_dir: str
    checkpoint_path: Optional[str] = None
    pretrain_checkpoint_path: Optional[str] = None


@dataclass
class AttackConfiguration:
    attack_norm: Union[str, int]
    random_start: bool
    iters: int
    epsilon: float
    alpha: float
    min_val: float
    max_val: float
    attack_to_branch: str


@dataclass
class FinetuneAttackConfiguration:
    attack_norm: Union[str, int]
    random_start: bool
    iters: int
    epsilon: float
    alpha: float
    min_val: float
    max_val: float
    auto_attack: bool


@dataclass
class SwAVPGDAttackConfiguration(AttackConfiguration):
    include_natural: bool
    include_natural_in_attack: bool


@dataclass
class SimCLRPGDAttackConfiguration(AttackConfiguration):
    """
    data_aug1 is attacked.
    The target for this attack is either:
        * data_aug1 ("self")
        or
        * data_aug2 ("other")
    A regularization loss is also calculated afterwards between adv_data and either:
        * data_aug1 ("self")
        or
        * data_aug2 ("other")
    """

    attack_target: str  # "self", "other"
    regularize_to: str  # "self", "other"
    reg_weight: float = 1 / 256
    xent_weight: float = 1.0


@dataclass
class BaseConfiguration:
    seed: int
    use_dual_bn: bool
    batch_size: int
    model: str
    cifar_stem: bool
    dataset_path: str
    epochs: int
    resume: bool
    run_id: str
    log_images: bool
    save_frequency: int
    directories: DirectoryConfiguration
    optimizer_cfg: OptimizerConfiguration
    lr_scheduler_cfg: LRSchedulerConfiguration
    augmentation_cfg: AugmentationConfiguration
    adv_training: bool
    use_amp: bool
    adv_cfg: Optional[
        Union[
            AttackConfiguration,
            FinetuneAttackConfiguration,
        ]
    ]


@dataclass
class FinetuneConfiguration(BaseConfiguration):
    encoder_trainable: bool
    pretrain_checkpoint_path: str
    adv_statistics: bool
    use_validation: bool
    percentage_labeled_data: int
    adv_cfg: Optional[FinetuneAttackConfiguration]
    use_frozen_features: bool
    # TODO: Not related to finetune
    visualize: bool
    visualization_dimension: Optional[int]
    tsne_fig_size: Optional[Tuple[int, int]]
    #
    use_natural_bn: bool = False
    # Related to trades in finetune
    trades: bool = False
    beta: float = 6.0


@dataclass
class PretrainConfiguration(BaseConfiguration):
    method: str
    method_cfg: MethodConfiguration
    knn_statistics: bool
    knn_freq: int


@dataclass
class SemiSupervisedConfiguration(BaseConfiguration):
    alpha_coefficient: float
    beta: float
    rate_distill: float
    temperature: float
    percentage_labeled_data: int
    pseudo_labels_path: str


@dataclass
class PseudoLabelConfiguration:
    augmentation_cfg: AugmentationConfiguration
    batch_size: int
    seed: int
    model: str
    cifar_stem: bool
    checkpoint_path: str
    output_path: str
    dataset_path: str
    use_dual_bn: bool
    use_natural_bn: bool


@dataclass
class TesterConfiguration:
    augmentation_cfg: AugmentationConfiguration
    batch_size: int
    seed: int
    model: str
    cifar_stem: bool
    checkpoint_path: str
    dataset_path: str
    use_dual_bn: bool
    adv_cfg: FinetuneAttackConfiguration
