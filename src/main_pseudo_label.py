import numpy as np
from tqdm import tqdm

from src.arguments import init_pseudo_label_argparse
from src.configuration import (
    AugmentationConfiguration,
    PseudoLabelConfiguration,
)
from src.initialize import prepare_cudnn, set_seed
from src.methods.pseudo_label import PseudoLabel


def main():
    parser = init_pseudo_label_argparse()
    args = parser.parse_args()
    # Prepare Environment
    set_seed(seed=args.seed)
    prepare_cudnn(deterministic=True, benchmark=True)

    augmentation_configuration = AugmentationConfiguration(
        dataset=args.dataset,
        image_size=args.image_size,
        use_random_resized_crop=args.use_random_resized_crop,
        use_random_crop=args.use_random_crop,
        use_resize=args.use_resize,
        use_gaussian_blur=args.use_gaussian_blur,
        normalize=args.normalize,
        use_color_jitter=args.use_color_jitter,
        use_grayscale=args.use_grayscale,
        min_scale_crops=args.min_scale_crops,
        max_scale_crops=args.max_scale_crops,
        color_jitter_strength=args.color_jitter_strength,
        grayscale_prob=args.grayscale_prob,
    )

    configuration = PseudoLabelConfiguration(
        seed=args.seed,
        model=args.model,
        cifar_stem=True,
        output_path=args.output_path,
        checkpoint_path=args.checkpoint_path,
        batch_size=args.batch_size,
        augmentation_cfg=augmentation_configuration,
        dataset_path=args.dataset_path,
        use_dual_bn=args.use_dual_bn,
        use_natural_bn=args.use_natural_bn,
    )

    pseudo_labeler = PseudoLabel(cfg=configuration)
    pseudo_labels = pseudo_labeler.extract_pseudo_labels()
    np.save(configuration.output_path, np.array(pseudo_labels))

    tqdm.write(
        "Pseudo labels extracted and saved to:\n{}".format(configuration.output_path)
    )


if __name__ == "__main__":
    main()
