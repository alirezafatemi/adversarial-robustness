import os
from typing import Tuple

import numpy as np
import torch
import torch.nn.functional as F
from matplotlib import pyplot as plt
from sklearn.manifold import TSNE
from torch import Tensor
from torch.nn import Module
from torch.utils.data import DataLoader


def adapt_model(model: torch.nn.Module, trainable: bool = False):
    for param in model.parameters():
        param.requires_grad = trainable
    return model


def update_moving_average(
    moving_average_decay: float, target_model: Module, online_model: Module
):
    for online_model_params, target_model_params in zip(
        online_model.parameters(), target_model.parameters()
    ):
        target_model_weight, online_model_weight = (
            target_model_params.data,
            online_model_params.data,
        )
        target_model_params.data = (
            target_model_weight * moving_average_decay
            + (1 - moving_average_decay) * online_model_weight
        )


def select_device() -> str:
    if torch.cuda.is_available():
        return "cuda"
    else:
        return "cpu"


def accuracy(output: Tensor, target: Tensor, topk=(1,)):
    """Computes the accuracy over the k top predictions for the specified values of k"""
    with torch.no_grad():
        maxk = max(topk)
        batch_size = target.shape[0]

        _, pred = output.topk(maxk, 1, True, True)
        pred = pred.t()
        correct = pred.eq(target.view(1, -1).expand_as(pred))

        res = []
        for k in topk:
            correct_k = correct[:k].view(-1).float().sum(0, keepdim=True)
            res.append(correct_k.mul_(100.0 / batch_size))
        return res


class AverageMeter:
    """Computes and stores the average and current value
    Imported from https://github.com/pytorch/examples/blob/master/imagenet/main.py#L247-L262
    """

    val: float
    avg: float
    sum: float
    count: int

    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val: float, n: int = 1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count


# Code copied from https://colab.research.google.com/github/facebookresearch/moco/blob/colab-notebook/colab/moco_cifar10_demo.ipynb#scrollTo=RI1Y8bSImD7N
# Test using a knn monitor
def knn_monitor(
    encoder: Module,
    train_data_loader: DataLoader,
    test_data_loader: DataLoader,
    k: int = 200,
    t: float = 0.1,
):
    encoder.eval()
    classes = len(train_data_loader.dataset.classes)
    total_top1, total_num, feature_bank = 0.0, 0, []
    with torch.no_grad():
        # Generate feature bank
        for data, target in train_data_loader:
            feature = encoder(data.cuda(non_blocking=True))
            feature = F.normalize(feature, dim=1)
            feature_bank.append(feature)
        # [D, N]
        feature_bank = torch.cat(feature_bank, dim=0).t().contiguous()
        # [N]
        feature_labels = torch.tensor(
            train_data_loader.dataset.targets, device=feature_bank.device
        )
        # Loop test data to predict the label by weighted knn search
        for data, target in test_data_loader:
            data, target = data.cuda(non_blocking=True), target.cuda(non_blocking=True)
            feature = encoder(data)
            feature = F.normalize(feature, dim=1)
            pred_labels = knn_predict(
                feature, feature_bank, feature_labels, classes, k, t
            )
            total_num += data.size(0)
            total_top1 += (pred_labels[:, 0] == target).float().sum().item()
    return total_top1 / total_num * 100


# knn monitor as in InstDisc https://arxiv.org/abs/1805.01978
# Implementation follows http://github.com/zhirongw/lemniscate.pytorch and https://github.com/leftthomas/SimCLR
def knn_predict(
    feature: torch.Tensor,
    feature_bank: torch.Tensor,
    feature_labels: torch.Tensor,
    classes: int,
    knn_k: int,
    knn_t: float,
):
    # Compute cos similarity between each feature vector and feature bank ---> [B, N]
    sim_matrix = torch.mm(feature, feature_bank)
    # [B, K]
    sim_weight, sim_indices = sim_matrix.topk(k=knn_k, dim=-1)
    # [B, K]
    sim_labels = torch.gather(
        feature_labels.expand(feature.size(0), -1), dim=-1, index=sim_indices
    )
    sim_weight = (sim_weight / knn_t).exp()

    # counts for each class
    one_hot_label = torch.zeros(
        feature.size(0) * knn_k, classes, device=sim_labels.device
    )
    # [B*K, C]
    one_hot_label = one_hot_label.scatter(
        dim=-1, index=sim_labels.view(-1, 1), value=1.0
    )
    # weighted score ---> [B, C]
    pred_scores = torch.sum(
        one_hot_label.view(feature.size(0), -1, classes) * sim_weight.unsqueeze(dim=-1),
        dim=1,
    )

    pred_labels = pred_scores.argsort(dim=-1, descending=True)
    return pred_labels


def tsne_visualize(
    features: np.array,
    targets: np.array,
    num_classes: int = 10,
    visualization_dimension: int = 2,
    fig_size: Tuple[int, int] = (6, 5),
):
    tsne = TSNE(n_components=visualization_dimension, random_state=0, n_jobs=-1)
    fitted_features = tsne.fit_transform(features)
    target_ids = range(num_classes)

    plt.figure(figsize=fig_size)
    colors = "r", "g", "b", "c", "m", "y", "k", "plum", "orange", "purple"
    for i, c in zip(target_ids, colors):
        plt.scatter(
            fitted_features[targets == i, 0],
            fitted_features[targets == i, 1],
            c=c,
            label=i + 1,
        )
    plt.legend()
    img_save_dir = "/content/images"
    if not os.path.exists(img_save_dir):
        os.makedirs(img_save_dir)
    plt.savefig(os.path.join(img_save_dir, "tsne.png"))


@torch.no_grad()
def inference(
    loader: DataLoader, model: Module, device: str
) -> Tuple[np.array, np.array]:
    feature_vector = []
    labels_vector = []
    for data, target in loader:
        features = model(data.to(device))

        features = features.squeeze()
        features = features.detach()

        feature_vector.extend(features.cpu().detach().numpy())
        labels_vector.extend(target.numpy())

    feature_vector = np.array(feature_vector)
    labels_vector = np.array(labels_vector)
    return feature_vector, labels_vector


def get_features(
    model: Module, train_loader: DataLoader, test_loader: DataLoader, device: str
) -> Tuple[np.array, np.array, np.array, np.array]:
    train_features, train_targets = inference(train_loader, model, device)
    test_features, test_targets = inference(test_loader, model, device)
    return train_features, train_targets, test_features, test_targets


class Logger:
    def __init__(self, path: str) -> None:
        self.path = path

    def info(self, msg: str) -> None:
        with open(os.path.join(self.path, "log.txt"), "a") as f:
            f.write(msg + "\n")
