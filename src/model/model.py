import math
from typing import Tuple

import torch
import torchvision.models as models
from torch import Tensor
from torch.nn import (
    BatchNorm1d,
    Conv2d,
    Identity,
    Linear,
    Module,
    ModuleList,
    ReLU,
    Sequential,
)
from torch.nn.functional import normalize

from src.configuration import (
    BYOLCLRConfiguration,
    BYOLConfiguration,
    SimCLRConfiguration,
    SimSiamConfiguration,
    SwAVConfiguration,
)
from .resnet import resnet18 as dual_resnet18

"""
Nobody uses ReLU in the final layer.
I guess the bias in Linear layers doesn't matter that much.

SimCLR & ACL:
    * 3 layers
    * BatchNorm1d after each Linear
    * Linear layers don't have bias
    * In SimCLR:
        * out_dim = 128
        * hidden_dim = in_dim
    * In ACL:
        * out_dim = in_dim (Doesn't change dimensions!)
        * hidden_dim = in_dim
    * (in_dim , hidden_dim) -> BatchNorm1d -> ReLU -> (hidden_dim , hidden_dim) -> BatchNorm1d -> ReLU -> (hidden_dim , out_dim) -> BatchNorm1d
    * In SimCLR: LR = base_lr * (batch_size/256) at the end of warmup
    
RoCL Projector:
    * 2 layers
    * All Linear layers have bias
    * No BatchNorm1d
    * Final layer has no ReLU
    * out_dim = 128
    * hidden_dim = 2048
    * (in_dim , hidden_dim) -> ReLU -> (hidden_dim , out_dim)

BYORL & BYOL:
    * The projector and the predictor have the same architecture:
    * 2 layers
    * hidden_dim = 4096
    * out_dim = 256
    * (in_dim , hidden_dim) -> BatchNorm1d -> ReLU -> (hidden_dim , out_dim)
    * Unlike SimCLR, output of the MLP is not batch normalized
    * ImageNet Settings:
        * LARS
        * Cosine decay learning rate schedule, without restarts, over 1000 epochs, with a warm-up period of 10 epochs
        * Base learning rate = 0.2, scaled linearly with the batch size (LearningRate = 0.2 × BatchSize/256)
        * Global weight decay = 1.5 · 10−6 while excluding the biases and batch normalization parameters from both LARS adaptation and weight decay
        * Exponential moving average parameter τ starts from τbase = 0.996 and is increased
            to one during training. Specifically, we set τ = 1 − (1 − τbase) · (cos(πk/K) + 1)/2 with k the current training
            step and K the maximum number of training steps.
        * We note that removing the weight decay
            in either BYOL or SimCLR leads to network divergence, emphasizing the need for weight regularization in the
            self-supervised setting.
        * In both training and evaluation, we normalize
            color channels by subtracting the average color and dividing by the standard deviation, computed on ImageNet,
            after applying the augmentations.
        * During evaluation: We optimize the cross-entropy loss using SGD with Nesterov momentum over
            80 epochs, using a batch size of 1024 and a momentum of 0.9. We do not use any regularization methods such
            as weight decay, gradient clipping, tclip, or logits regularization.
        * They also have another linear evaluation protocol. Page 17 of paper
        * For batch_size = 512: Using the same linear evaluation setup, BYOL achieves 73.7% top-1 accuracy when trained over 1000
            epochs with a batch size of 512 split over 64 TPU cores (approximately 4 days of training). For this setup, we reuse
            the same setting as in Section 3, but use a base learning rate of 0.4 (appropriately scaled by the batch size) and
            τbase = 0.9995 with the same weight decay coefficient of 1.5 · 10−6.  

SimCLR and BYOL in https://github.com/htdt/self-supervised:
    * 2 layers
    * out_dim = 64 for CIFAR-10 & 100
    * out_dim = 128 for STL-10 & Tiny ImageNet
    * hidden_dim = 1024
    * All Linear layers have bias
    * BYOL:
        * (in_dim , hidden_dim) -> BatchNorm1d -> ReLU -> (hidden_dim , out_dim)
    * SimCLR:
        * (in_dim , hidden_dim) -> BatchNorm1d -> ReLU -> (hidden_dim , out_dim) -> BatchNorm1d
    * Moving average is 0.99 with cosine increasing schedule

SwAV:
    * 2 layers
    * For ImageNet:
        * out_dim = 128
        * hidden_dim = 2048
    * All Linear layers have bias
    * (in_dim , hidden_dim) -> BatchNorm1d -> ReLU -> (hidden_dim , out_dim)

SimSiam:
    * The projection MLP (in f) has BN applied to each fully-connected (fc) layer, including its output fc.
    * It's output fc has no ReLU. The hidden fc is 2048-d. This MLP has 3 layers.
    * The prediction MLP (h) has BN applied to its hidden fc layers. 
    * Its output fc does not have BN or ReLU. This MLP has 2 layers.
    * The dimension of h’s input and output (z and p) is d = 2048, and h’s hidden layer’s dimension is 512, making h a bottleneck structure.
    * Experiments from paper on out_dim or d:
        output d 256 512 1024 2048
        acc. (%) 65.3 67.2 67.5 68.1
        The prediction MLP’s hidden layer dimension is always 1/4 of the output dimension.
    * Projector:
        * 2 layers for Cifar10 and 3 layers for others
        * out_dim = 2048
        * hidden_dim = 2048
        * For 3 layers:
            * (in_dim , hidden_dim) -> BatchNorm1d -> ReLU -> (hidden_dim , hidden_dim) -> BatchNorm1d -> ReLU-> (hidden_dim , out_dim) -> BatchNorm1d
        * For 2 layers:
            * (in_dim , hidden_dim)-> BatchNorm1d -> ReLU -> (hidden_dim , out_dim) -> BatchNorm1d
    * Predictor:
        * 2 layers for all
        * in_dim = 2048
        * out_dim = 2048
        * hidden_dim = 512
        * (in_dim , hidden_dim) -> BatchNorm1d -> ReLU -> (hidden_dim , out_dim)

base_lr for SimSiam = 0.03
weight_decay for SimSiam = 5e-4
base_lr for SimCLR & BYOL & SwAV = 0.3
weight_decay for SimCLR & BYOL & SwAV = 1e-6
SimSiam doesn't use warmup.
lr after warmup = base_lr * (batch_size / 256)  
"""


class DualBatchNorm(Module):
    def __init__(self, norm, inplanes):
        super(DualBatchNorm, self).__init__()
        self.adv_bn = norm(inplanes)
        self.natural_bn = norm(inplanes)

    def forward(self, x, is_natural: bool = False):
        if is_natural:
            return self.natural_bn(x)
        return self.adv_bn(x)


class MLP(Module):
    def __init__(
        self,
        in_dim: int,
        hidden_dim: int,
        out_dim: int,
        add_hidden_bn,
        add_out_bn: bool,
        num_layers: int,
        use_dual_bn: bool = False,
    ):
        super(MLP, self).__init__()
        self.use_dual_bn = use_dual_bn
        layers = []
        in_size = in_dim
        for _ in range(num_layers - 1):
            layers.append(Linear(in_size, hidden_dim, bias=not add_hidden_bn))
            if add_hidden_bn:
                if use_dual_bn:
                    layers.append(DualBatchNorm(BatchNorm1d, hidden_dim))
                else:
                    layers.append(BatchNorm1d(hidden_dim))
            layers.append(ReLU(inplace=True))
            in_size = hidden_dim
        layers.append(Linear(hidden_dim, out_dim, bias=not add_out_bn))
        if add_out_bn:
            if use_dual_bn:
                layers.append(DualBatchNorm(BatchNorm1d, out_dim))
            else:
                layers.append(BatchNorm1d(out_dim))
        if use_dual_bn:
            self.layers = ModuleList(layers)
        else:
            self.layers = Sequential(*layers)

    def forward(self, x, is_natural: bool = False):
        if self.use_dual_bn:
            for layer in self.layers:
                if isinstance(layer, DualBatchNorm):
                    x = layer(x, is_natural)
                else:
                    x = layer(x)
            return x
        return self.layers(x)


class Encoder(Module):
    def __init__(
        self,
        model: str = "resnet18",
        cifar_stem: bool = True,
        use_dual_bn: bool = False,
    ):
        super(Encoder, self).__init__()
        self.use_dual_bn = use_dual_bn
        if model == "resnet18":
            if use_dual_bn:
                self.feature_extractor = dual_resnet18(bn_names=["adv", "natural"])
            else:
                self.feature_extractor = models.resnet18(pretrained=False)
        elif model == "resnet50":
            self.feature_extractor = models.resnet50(pretrained=False)
        else:
            raise NotImplementedError
        if cifar_stem:
            # Customize for CIFAR10. Replace conv 7x7 with conv 3x3, and remove first max pooling.
            self.feature_extractor.conv1 = Conv2d(
                in_channels=3,
                out_channels=64,
                kernel_size=3,
                stride=1,
                padding=1,
                bias=False,
            )
            self.feature_extractor.maxpool = Identity()
        self.out_size = self.feature_extractor.fc.in_features
        self.feature_extractor.fc = Identity()

    def forward(self, x: Tensor, is_natural: bool = False) -> Tensor:
        if self.use_dual_bn:
            if is_natural:
                feature_vectors = self.feature_extractor(x, "natural")
            else:
                feature_vectors = self.feature_extractor(x, "adv")
        else:
            feature_vectors = self.feature_extractor(x)
        return feature_vectors


class BaseModel(Module):
    def __init__(self, model: str, cifar_stem: bool, use_dual_bn: bool = False):
        super(BaseModel, self).__init__()
        self.encoder = Encoder(
            model=model, cifar_stem=cifar_stem, use_dual_bn=use_dual_bn
        )

    def extract_features(self, x: Tensor, is_natural: bool = False) -> Tensor:
        return self.encoder(x, is_natural)


class FinetuneModel(BaseModel):
    def __init__(
        self, model: str, cifar_stem: bool, num_classes: int, use_dual_bn: bool = False
    ):
        super(FinetuneModel, self).__init__(model, cifar_stem, use_dual_bn)
        self.linear = Linear(self.encoder.out_size, num_classes)

    def forward(self, x: Tensor, is_natural: bool = False) -> Tensor:
        return self.linear(self.encoder(x, is_natural).squeeze())


class SimSiam(BaseModel):
    def __init__(
        self,
        model: str,
        cifar_stem: bool,
        method_cfg: SimSiamConfiguration,
        use_dual_bn: bool = False,
    ):
        super(SimSiam, self).__init__(model, cifar_stem, use_dual_bn)
        self.projector = MLP(
            in_dim=self.encoder.out_size,
            hidden_dim=method_cfg.projection_hidden_size,
            out_dim=method_cfg.projection_size,
            num_layers=method_cfg.num_projection_layers,
            add_hidden_bn=method_cfg.projector_hidden_bn,
            add_out_bn=method_cfg.projector_out_bn,
            use_dual_bn=use_dual_bn,
        )
        self.predictor = MLP(
            in_dim=method_cfg.projection_size,
            hidden_dim=method_cfg.prediction_hidden_size,
            out_dim=method_cfg.projection_size,
            num_layers=method_cfg.num_prediction_layers,
            add_hidden_bn=method_cfg.predictor_hidden_bn,
            add_out_bn=method_cfg.predictor_out_bn,
            use_dual_bn=use_dual_bn,
        )

    def forward(self, x: Tensor, is_natural: bool = False) -> Tuple[Tensor, Tensor]:
        projection = self.projector(self.encoder(x, is_natural), is_natural)
        prediction = self.predictor(projection, is_natural)
        return projection, prediction

    def extract_projections(self, x: Tensor, is_natural: bool = False) -> Tensor:
        return self.projector(self.encoder(x, is_natural), is_natural)

    def extract_predictions(self, x: Tensor, is_natural: bool = False) -> Tensor:
        return self.predictor(
            self.projector(self.encoder(x, is_natural), is_natural), is_natural
        )


class SimCLR(BaseModel):
    def __init__(
        self,
        model: str,
        cifar_stem: bool,
        method_cfg: SimCLRConfiguration,
        use_dual_bn: bool = False,
    ):
        super(SimCLR, self).__init__(model, cifar_stem, use_dual_bn)
        self.projector = MLP(
            in_dim=self.encoder.out_size,
            hidden_dim=method_cfg.projection_hidden_size,
            out_dim=method_cfg.projection_size,
            num_layers=method_cfg.num_projection_layers,
            add_hidden_bn=method_cfg.projector_hidden_bn,
            add_out_bn=method_cfg.projector_out_bn,
            use_dual_bn=use_dual_bn,
        )

    def forward(self, x: Tensor, is_natural: bool = False) -> Tensor:
        return self.projector(self.encoder(x, is_natural), is_natural)


class BYOL(BaseModel):
    def __init__(
        self,
        model: str,
        cifar_stem: bool,
        method_cfg: BYOLConfiguration,
        use_dual_bn: bool = False,
    ):
        super(BYOL, self).__init__(model, cifar_stem, use_dual_bn)
        self.method_cfg = method_cfg
        self.base_moving_average_decay = method_cfg.moving_average_decay
        self.current_moving_average_decay = method_cfg.moving_average_decay
        self.projector = MLP(
            in_dim=self.encoder.out_size,
            hidden_dim=method_cfg.projection_hidden_size,
            out_dim=method_cfg.projection_size,
            num_layers=method_cfg.num_projection_layers,
            add_hidden_bn=method_cfg.projector_hidden_bn,
            add_out_bn=method_cfg.projector_out_bn,
            use_dual_bn=use_dual_bn,
        )
        self.predictor = MLP(
            in_dim=method_cfg.projection_size,
            hidden_dim=method_cfg.prediction_hidden_size,
            out_dim=method_cfg.projection_size,
            num_layers=method_cfg.num_prediction_layers,
            add_hidden_bn=method_cfg.predictor_hidden_bn,
            add_out_bn=method_cfg.predictor_out_bn,
            use_dual_bn=use_dual_bn,
        )

        self.target_encoder = Encoder(
            model=model, cifar_stem=cifar_stem, use_dual_bn=use_dual_bn
        )
        self.target_projector = MLP(
            in_dim=self.target_encoder.out_size,
            hidden_dim=method_cfg.projection_hidden_size,
            out_dim=method_cfg.projection_size,
            num_layers=method_cfg.num_projection_layers,
            add_hidden_bn=method_cfg.projector_hidden_bn,
            add_out_bn=method_cfg.projector_out_bn,
            use_dual_bn=use_dual_bn,
        )

        self.target_encoder.load_state_dict(self.encoder.state_dict())
        self.target_projector.load_state_dict(self.projector.state_dict())

        for param in self.target_encoder.parameters():
            param.requires_grad = False
        for param in self.target_projector.parameters():
            param.requires_grad = False

    def forward(
        self, x_online: Tensor, x_target: Tensor, is_natural: bool = False
    ) -> Tuple[Tensor, Tensor]:
        return self.forward_online(x_online, is_natural), self.forward_target(
            x_target, is_natural
        )

    def forward_target(self, x: Tensor, is_natural: bool = False) -> Tensor:
        target_features = self.target_encoder(x, is_natural)
        target_projection = self.target_projector(target_features, is_natural)
        return target_projection

    def forward_online(self, x: Tensor, is_natural: bool = False) -> Tensor:
        online_features = self.encoder(x, is_natural)
        online_projection = self.projector(online_features, is_natural)
        online_prediction = self.predictor(online_projection, is_natural)
        return online_prediction

    @torch.no_grad()
    def update_moving_average(self, epoch: int, total_epochs: int):
        moving_average_decay = self.base_moving_average_decay
        if self.method_cfg.use_cosine_increasing_schedule:
            progress = epoch / total_epochs
            moving_average_decay = (
                1 - (1 - moving_average_decay) * (math.cos(math.pi * progress) + 1) / 2
            )
        self.current_moving_average_decay = moving_average_decay
        for online_model_params, target_model_params in zip(
            self.encoder.parameters(), self.target_encoder.parameters()
        ):
            target_model_weight, online_model_weight = (
                target_model_params.data,
                online_model_params.data,
            )
            target_model_params.data = (
                target_model_weight * moving_average_decay
                + (1 - moving_average_decay) * online_model_weight
            )
        for online_model_params, target_model_params in zip(
            self.projector.parameters(), self.target_projector.parameters()
        ):
            target_model_weight, online_model_weight = (
                target_model_params.data,
                online_model_params.data,
            )
            target_model_params.data = (
                target_model_weight * moving_average_decay
                + (1 - moving_average_decay) * online_model_weight
            )


class SwAV(BaseModel):
    def __init__(
        self,
        model: str,
        cifar_stem: bool,
        method_cfg: SwAVConfiguration,
        l2norm: bool = True,
        use_dual_bn: bool = False,
    ):
        super(SwAV, self).__init__(model, cifar_stem, use_dual_bn)
        self.projector = MLP(
            in_dim=self.encoder.out_size,
            hidden_dim=method_cfg.projection_hidden_size,
            out_dim=method_cfg.projection_size,
            num_layers=method_cfg.num_projection_layers,
            add_hidden_bn=method_cfg.projector_hidden_bn,
            add_out_bn=method_cfg.projector_out_bn,
            use_dual_bn=use_dual_bn,
        )
        self.prototypes = Linear(
            method_cfg.projection_size, method_cfg.num_prototypes, bias=False
        )
        # Normalize output features
        self.l2norm = l2norm

    def normalize_prototypes(self):
        with torch.no_grad():
            w = self.prototypes.weight.data.clone()
            w = normalize(w, dim=1, p=2)
            self.prototypes.weight.copy_(w)

    def forward(self, x, is_natural: bool = False):
        projections = self.projector(self.encoder(x, is_natural), is_natural)
        if self.l2norm:
            projections = normalize(projections, dim=1, p=2)
        return self.prototypes(projections)


class BYOLCLR(BYOL):
    def __init__(
        self,
        model: str,
        cifar_stem: bool,
        method_cfg: BYOLCLRConfiguration,
        use_dual_bn: bool = False,
    ):
        super(BYOL, self).__init__(model, cifar_stem, use_dual_bn)
        self.method_cfg = method_cfg
        self.base_moving_average_decay = method_cfg.moving_average_decay
        self.current_moving_average_decay = method_cfg.moving_average_decay
        self.projector = MLP(
            in_dim=self.encoder.out_size,
            hidden_dim=method_cfg.projection_hidden_size,
            out_dim=method_cfg.projection_size,
            num_layers=method_cfg.num_projection_layers,
            add_hidden_bn=method_cfg.projector_hidden_bn,
            add_out_bn=method_cfg.projector_out_bn,
            use_dual_bn=use_dual_bn,
        )

        self.target_encoder = Encoder(
            model=model, cifar_stem=cifar_stem, use_dual_bn=use_dual_bn
        )
        self.target_projector = MLP(
            in_dim=self.target_encoder.out_size,
            hidden_dim=method_cfg.projection_hidden_size,
            out_dim=method_cfg.projection_size,
            num_layers=method_cfg.num_projection_layers,
            add_hidden_bn=method_cfg.projector_hidden_bn,
            add_out_bn=method_cfg.projector_out_bn,
            use_dual_bn=use_dual_bn,
        )

        self.target_encoder.load_state_dict(self.encoder.state_dict())
        self.target_projector.load_state_dict(self.projector.state_dict())

        for param in self.target_encoder.parameters():
            param.requires_grad = False
        for param in self.target_projector.parameters():
            param.requires_grad = False

    def forward(
        self, x_online: Tensor, x_target: Tensor, is_natural: bool = False
    ) -> Tuple[Tensor, Tensor]:
        return self.forward_online(x_online, is_natural), self.forward_target(
            x_target, is_natural
        )

    def forward_target(self, x: Tensor, is_natural: bool = False) -> Tensor:
        target_features = self.target_encoder(x, is_natural)
        target_projection = self.target_projector(target_features, is_natural)
        return target_projection

    @torch.no_grad()
    def update_moving_average(self, epoch: int, total_epochs: int):
        moving_average_decay = self.base_moving_average_decay
        if self.method_cfg.use_cosine_increasing_schedule:
            progress = epoch / total_epochs
            moving_average_decay = (
                1 - (1 - moving_average_decay) * (math.cos(math.pi * progress) + 1) / 2
            )
        self.current_moving_average_decay = moving_average_decay
        for online_model_params, target_model_params in zip(
            self.encoder.parameters(), self.target_encoder.parameters()
        ):
            target_model_weight, online_model_weight = (
                target_model_params.data,
                online_model_params.data,
            )
            target_model_params.data = (
                target_model_weight * moving_average_decay
                + (1 - moving_average_decay) * online_model_weight
            )
        for online_model_params, target_model_params in zip(
            self.projector.parameters(), self.target_projector.parameters()
        ):
            target_model_weight, online_model_weight = (
                target_model_params.data,
                online_model_params.data,
            )
            target_model_params.data = (
                target_model_weight * moving_average_decay
                + (1 - moving_average_decay) * online_model_weight
            )

    def forward_online(self, x: Tensor, is_natural: bool = False) -> Tensor:
        online_features = self.encoder(x, is_natural)
        online_projection = self.projector(online_features, is_natural)
        return online_projection
