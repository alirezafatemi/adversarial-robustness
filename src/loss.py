import torch
from torch import Tensor


def scaled_pairwise_similarity(outputs: Tensor, temperature: float = 0.5) -> Tensor:
    """
    Compute scaled pairwise similarity and return the matrix
    input: aggregated outputs & temperature for scaling
    return: pairwise cosine similarity
    """
    outputs_norm = outputs / (outputs.norm(dim=1).view(outputs.shape[0], 1) + 1e-8)
    similarity_matrix = (1.0 / temperature) * torch.mm(
        outputs_norm, outputs_norm.transpose(0, 1).detach()
    )
    return similarity_matrix


# From https://github.com/wangxin0716/SimCLR-CIFAR10
def nt_xent(x: torch.Tensor, temperature: float = 0.5) -> torch.Tensor:
    scaled_similarity_matrix = scaled_pairwise_similarity(x, temperature)
    sim_size = len(scaled_similarity_matrix)
    batch_size = int(sim_size / 2)

    similarity_matrix_exp = torch.exp(scaled_similarity_matrix)
    # Removing diagonal
    similarity_matrix_exp = (
        similarity_matrix_exp * (1 - torch.eye(sim_size, sim_size)).cuda()
    )

    # Denominator of loss
    # shape = sim_size * sim_size
    nt_xent_loss = -torch.log(
        similarity_matrix_exp
        / (torch.sum(similarity_matrix_exp, dim=1).view(sim_size, 1) + 1e-8)
        + 1e-8
    )

    # diag of 2 and 3 are pairs of samples that we want to be similar
    # [ 1 2 ]
    # [ 3 4 ]
    nt_xent_loss_total = (1.0 / float(sim_size)) * torch.sum(
        #           2
        torch.diag(nt_xent_loss[0:batch_size, batch_size:])
        #           3
        + torch.diag(nt_xent_loss[batch_size:, 0:batch_size])
    )
    return nt_xent_loss_total


def adv_nt_xent(x: torch.Tensor, temperature: float = 0.5) -> torch.Tensor:
    scaled_similarity_matrix = scaled_pairwise_similarity(x, temperature)
    sim_size = len(scaled_similarity_matrix)
    batch_size = int(len(scaled_similarity_matrix) / 3)

    similarity_matrix_exp = torch.exp(scaled_similarity_matrix)
    # Remove diagonal
    similarity_matrix_exp = (
        similarity_matrix_exp * (1 - torch.eye(sim_size, sim_size)).cuda()
    )

    # Denominator of loss
    # shape = sim_size * sim_size
    nt_xent_loss = -torch.log(
        similarity_matrix_exp
        / (torch.sum(similarity_matrix_exp, dim=1).view(sim_size, 1) + 1e-8)
        + 1e-8
    )
    # diag of 2, 3, 4, 6, 7, and 8 are pairs of samples that we want to be similar
    # [ 1 2 3 ]
    # [ 4 5 6 ]
    # [ 7 8 9 ]
    nt_xent_loss_total = (1.0 / float(sim_size)) * torch.sum(
        #               2
        torch.diag(nt_xent_loss[0:batch_size, batch_size : 2 * batch_size])
        #               4
        + torch.diag(nt_xent_loss[batch_size : 2 * batch_size, 0:batch_size])
        #               3
        + torch.diag(nt_xent_loss[0:batch_size, 2 * batch_size :])
        #               7
        + torch.diag(nt_xent_loss[2 * batch_size :, 0:batch_size])
        #               6
        + torch.diag(nt_xent_loss[batch_size : 2 * batch_size, 2 * batch_size :])
        #               8
        + torch.diag(nt_xent_loss[2 * batch_size :, batch_size : 2 * batch_size])
    )
    return nt_xent_loss_total
