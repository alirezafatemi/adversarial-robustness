from typing import List, Tuple

import torch
from torch import Tensor
from torch.nn.functional import normalize

from src.adversary.adversary import apply_grads, get_grads, random_start
from src.configuration import (
    BYOLConfiguration,
    AttackConfiguration,
    PretrainConfiguration,
)
from src.loss import adv_nt_xent, nt_xent
from src.methods.pretrain_base import PretrainBaseMethod
from src.model.model import BYOL
from src.utils import AverageMeter


class BYOLMethodPretrain(PretrainBaseMethod):
    model: BYOL

    def __init__(self, cfg: PretrainConfiguration) -> None:
        assert isinstance(cfg.method_cfg, BYOLConfiguration)
        super().__init__(cfg)

    def prepare_model(self) -> None:
        assert isinstance(self.cfg.method_cfg, BYOLConfiguration)
        self.model = BYOL(
            model=self.cfg.model,
            cifar_stem=self.cfg.cifar_stem,
            method_cfg=self.cfg.method_cfg,
            use_dual_bn=self.cfg.use_dual_bn,
        )

    def update_moving_average_decay_stats(self, moving_average_decay: float) -> None:
        self.stats.update({"moving_average_decay": moving_average_decay})

    def extract_online_predictions(self, views: Tuple[Tensor, ...]) -> List[Tensor]:
        online_predictions = []
        for view in views:
            online_prediction = self.model.forward_online(view)
            online_predictions.append(online_prediction)
        return online_predictions

    def extract_target_projections(
        self, views: Tuple[Tensor, ...], detach_targets: bool = True
    ) -> List[Tensor]:
        target_projections = []
        for view in views:
            target_projection = self.model.forward_target(view)
            if detach_targets:
                target_projection = target_projection.detach()  # stop gradient
            target_projections.append(target_projection)
        return target_projections

    def calculate_loss(
        self, online_predictions: List[Tensor], target_projections: List[Tensor]
    ) -> Tensor:
        loss, count = 0.0, 0
        for idx, online_prediction in enumerate(online_predictions):
            for other_projection in (
                target_projections[:idx] + target_projections[idx + 1 :]
            ):
                loss += byol_loss_function(online_prediction, other_projection)
                count += 1
        loss /= count
        return loss

    def pretrain_step(self):
        epoch = self.current_epoch
        loss_meter = AverageMeter()

        for data_aug1, data_aug2 in self.pretrain_loader:
            data_aug1, data_aug2 = (
                data_aug1.to(self.device),
                data_aug2.to(self.device),
            )

            self.model.train()
            views = (data_aug1, data_aug2)
            online_predictions = self.extract_online_predictions(views)
            target_projections = self.extract_target_projections(views)
            loss = self.calculate_loss(online_predictions, target_projections)

            self.optimize(loss)

            self.model.update_moving_average(epoch=epoch, total_epochs=self.cfg.epochs)
            loss_meter.update(loss.item())

        self.update_loss_stats(loss_meter.avg)
        self.update_moving_average_decay_stats(self.model.current_moving_average_decay)

    def pretrain_adv_step(self) -> None:
        assert self.cfg.adv_cfg is not None
        assert isinstance(self.cfg.adv_cfg, AttackConfiguration)
        assert isinstance(self.cfg.method_cfg, BYOLConfiguration)

        adv_cfg = self.cfg.adv_cfg

        epoch = self.current_epoch
        loss_meter = AverageMeter()

        for data_aug1, data_aug2 in self.pretrain_loader:
            data_aug1, data_aug2 = (
                data_aug1.to(self.device),
                data_aug2.to(self.device),
            )
            if adv_cfg.attack_to_branch == "both":
                adv_data_aug1_online, adv_data_aug2_target = self.attack_both(
                    online_data=data_aug1, target_data=data_aug2
                )
                # Flip
                adv_data_aug2_online, adv_data_aug1_target = self.attack_both(
                    online_data=data_aug2, target_data=data_aug1
                )
            elif adv_cfg.attack_to_branch == "online":
                adv_data_aug1 = self.attack_online(
                    online_data=data_aug1, target_data=data_aug2
                )
                # Flip
                adv_data_aug2 = self.attack_online(
                    online_data=data_aug2, target_data=data_aug1
                )
            elif adv_cfg.attack_to_branch == "target":
                adv_data_aug1 = self.attack_target(
                    online_data=data_aug2, target_data=data_aug1
                )
                # Flip
                adv_data_aug2 = self.attack_target(
                    online_data=data_aug1, target_data=data_aug2
                )
            elif adv_cfg.attack_to_branch == "rocl":
                adv_data_aug1 = self.attack_rocl(data_aug1, data_aug2)
            elif adv_cfg.attack_to_branch == "rocl2":
                adv_data_aug1_online = self.attack_online(
                    online_data=data_aug1, target_data=data_aug2
                )
                adv_data_aug1_target = self.attack_target(
                    online_data=data_aug2, target_data=data_aug1
                )
            elif adv_cfg.attack_to_branch == "dual_stream":
                # The loss is still symmetric.
                # Sum of one loss value calculated for perturbed samples and the other calculated for natural samples.
                adv_data_aug1_online, adv_data_aug2_target = self.attack_both(
                    online_data=data_aug1, target_data=data_aug2
                )
            else:
                raise ValueError

            self.model.train()

            if adv_cfg.attack_to_branch == "both":
                online_views = (adv_data_aug1_online, adv_data_aug2_online)
                target_views = (adv_data_aug1_target, adv_data_aug2_target)
            elif adv_cfg.attack_to_branch == "online":
                online_views = (adv_data_aug1, adv_data_aug2)
                target_views = (data_aug1, data_aug2)
            elif adv_cfg.attack_to_branch == "target":
                online_views = (data_aug1, data_aug2)
                target_views = (adv_data_aug1, adv_data_aug2)
            elif adv_cfg.attack_to_branch == "rocl":
                online_views = (adv_data_aug1, data_aug1, data_aug2)
                target_views = (adv_data_aug1, data_aug1, data_aug2)
            elif adv_cfg.attack_to_branch == "rocl2":
                online_views = (adv_data_aug1_online, data_aug1, data_aug2)
                target_views = (adv_data_aug1_target, data_aug1, data_aug2)
            elif adv_cfg.attack_to_branch == "dual_stream":
                online_views = (adv_data_aug1_online, data_aug2)
                target_views = (data_aug1, adv_data_aug2_target)

            online_predictions = self.extract_online_predictions(online_views)
            target_projections = self.extract_target_projections(target_views)

            if self.cfg.method_cfg.use_contrastive_loss_in_train:
                if adv_cfg.attack_to_branch == "rocl":
                    # loss = (
                    #     nt_xent(
                    #         torch.cat((online_predictions[0], target_projections[1]))
                    #     )
                    #     + nt_xent(
                    #         torch.cat((online_predictions[0], target_projections[2]))
                    #     )
                    #     + nt_xent(
                    #         torch.cat((online_predictions[1], target_projections[0]))
                    #     )
                    #     + nt_xent(
                    #         torch.cat((online_predictions[1], target_projections[2]))
                    #     )
                    #     + nt_xent(
                    #         torch.cat((online_predictions[2], target_projections[0]))
                    #     )
                    #     + nt_xent(
                    #         torch.cat((online_predictions[2], target_projections[1]))
                    #     )
                    # ) / 6

                    loss = (
                        adv_nt_xent(
                            torch.cat(
                                (
                                    online_predictions[0],
                                    target_projections[1],
                                    target_projections[2],
                                )
                            )
                        )
                        + adv_nt_xent(
                            torch.cat(
                                (
                                    online_predictions[1],
                                    target_projections[0],
                                    target_projections[2],
                                )
                            )
                        )
                        + adv_nt_xent(
                            torch.cat(
                                (
                                    online_predictions[2],
                                    target_projections[0],
                                    target_projections[1],
                                )
                            )
                        )
                    ) / 3
                else:
                    # Divide by two later!
                    loss = nt_xent(
                        torch.cat((online_predictions[0], target_projections[1]))
                    ) + nt_xent(
                        torch.cat((online_predictions[1], target_projections[0]))
                    )
            else:
                loss = self.calculate_loss(online_predictions, target_projections)

            self.optimize(loss)

            self.model.update_moving_average(epoch=epoch, total_epochs=self.cfg.epochs)
            loss_meter.update(loss.item())

        self.update_loss_stats(loss_meter.avg)
        self.update_moving_average_decay_stats(self.model.current_moving_average_decay)

    def attack_online(self, online_data: Tensor, target_data: Tensor) -> Tensor:
        assert isinstance(self.cfg.method_cfg, BYOLConfiguration)
        adv_cfg = self.cfg.adv_cfg

        if adv_cfg.random_start:
            adv_online_data = random_start(
                online_data,
                eps=adv_cfg.epsilon,
                norm=adv_cfg.attack_norm,
                min_val=adv_cfg.min_val,
                max_val=adv_cfg.max_val,
            )
        else:
            adv_online_data = online_data

        # Set model to eval mode
        self.model.eval()
        target_projection = self.extract_target_projections((target_data,))[0]
        # Generate Adversarial Sample
        for _ in range(adv_cfg.iters):
            self.model.zero_grad()
            adv_online_data = (
                adv_online_data.clone().detach().requires_grad_(True).to(self.device)
            )

            online_prediction = self.extract_online_predictions((adv_online_data,))[0]

            if self.cfg.method_cfg.use_contrastive_loss_in_attack:
                loss = nt_xent(torch.cat((online_prediction, target_projection)))
            else:
                loss = byol_loss_function(online_prediction, target_projection)

            grads = get_grads(outputs=loss, inputs=adv_online_data)
            adv_online_data = apply_grads(adv_online_data, online_data, grads, adv_cfg)

        return adv_online_data.detach()

    def attack_target(self, online_data: Tensor, target_data: Tensor) -> Tensor:
        assert isinstance(self.cfg.method_cfg, BYOLConfiguration)
        adv_cfg = self.cfg.adv_cfg

        if adv_cfg.random_start:
            adv_target_data = random_start(
                target_data,
                eps=adv_cfg.epsilon,
                norm=adv_cfg.attack_norm,
                min_val=adv_cfg.min_val,
                max_val=adv_cfg.max_val,
            )
        else:
            adv_target_data = target_data

        # Set model to eval mode
        self.model.eval()
        online_prediction = self.extract_online_predictions((online_data,))[0].detach()
        # Generate Adversarial Sample
        for _ in range(adv_cfg.iters):
            self.model.zero_grad()
            adv_target_data = (
                adv_target_data.clone().detach().requires_grad_(True).to(self.device)
            )

            target_projection = self.extract_target_projections(
                (adv_target_data,), detach_targets=False
            )[0]
            loss = byol_loss_function(online_prediction, target_projection)

            grads = get_grads(outputs=loss, inputs=adv_target_data)
            adv_target_data = apply_grads(adv_target_data, online_data, grads, adv_cfg)

        return adv_target_data.detach()

    def attack_both(
        self, online_data: Tensor, target_data: Tensor
    ) -> Tuple[Tensor, Tensor]:
        adv_cfg = self.cfg.adv_cfg

        if adv_cfg.random_start:
            adv_data_online = random_start(
                online_data,
                eps=adv_cfg.epsilon,
                norm=adv_cfg.attack_norm,
                min_val=adv_cfg.min_val,
                max_val=adv_cfg.max_val,
            )
            adv_data_target = random_start(
                target_data,
                eps=adv_cfg.epsilon,
                norm=adv_cfg.attack_norm,
                min_val=adv_cfg.min_val,
                max_val=adv_cfg.max_val,
            )
        else:
            adv_data_online = online_data
            adv_data_target = target_data

        # Set model to eval mode
        self.model.eval()
        for _ in range(adv_cfg.iters):
            self.model.zero_grad()
            adv_data_online = (
                adv_data_online.clone()
                .detach()
                .requires_grad_(True)
                .to(adv_data_online.device)
            )
            adv_data_target = (
                adv_data_target.clone()
                .detach()
                .requires_grad_(True)
                .to(adv_data_target.device)
            )
            # Forward
            online_prediction = self.extract_online_predictions((adv_data_online,))[0]
            target_projection = self.extract_target_projections(
                (adv_data_target,), detach_targets=False
            )[0]
            # Calculate Loss
            if self.cfg.method_cfg.use_contrastive_loss_in_attack:
                loss = nt_xent(torch.cat((online_prediction, target_projection)))
            else:
                loss = byol_loss_function(online_prediction, target_projection).mean()

            grads_online = get_grads(
                outputs=loss, inputs=adv_data_online, retain_graph=True
            )
            grads_target = get_grads(outputs=loss, inputs=adv_data_target)

            adv_data_online = apply_grads(
                adv_data_online, online_data, grads_online, adv_cfg
            )
            adv_data_target = apply_grads(
                adv_data_target, target_data, grads_target, adv_cfg
            )

        return adv_data_online.detach(), adv_data_target.detach()

    def attack_rocl(self, data_aug1: torch.Tensor, data_aug2: torch.Tensor) -> Tensor:
        assert isinstance(self.cfg.method_cfg, BYOLConfiguration)
        adv_cfg = self.cfg.adv_cfg

        if adv_cfg.random_start:
            adv_data_aug1 = random_start(
                data_aug1,
                eps=adv_cfg.epsilon,
                norm=adv_cfg.attack_norm,
                min_val=adv_cfg.min_val,
                max_val=adv_cfg.max_val,
            )
        else:
            adv_data_aug1 = data_aug1

        # Set model to eval mode
        self.model.eval()
        # Generate Adversarial Sample
        online_prediction_2 = self.extract_online_predictions((data_aug2,))[0]
        target_projection_2 = self.extract_target_projections((data_aug2,))[0]
        for _ in range(adv_cfg.iters):
            self.model.zero_grad()
            adv_data_aug1 = (
                adv_data_aug1.clone().detach().requires_grad_(True).to(self.device)
            )
            online_prediction_1 = self.extract_online_predictions((adv_data_aug1,))[0]
            target_projection_1 = self.extract_target_projections(
                (adv_data_aug1,), detach_targets=False
            )[0]
            if self.cfg.method_cfg.use_contrastive_loss_in_attack:
                # Divide by two later!
                loss = nt_xent(
                    torch.cat((online_prediction_1, target_projection_2))
                ) + nt_xent(torch.cat((online_prediction_2, target_projection_1)))
            else:
                loss = self.calculate_loss(
                    (online_prediction_1, online_prediction_2),
                    (target_projection_1, target_projection_2),
                )

            grads1 = get_grads(outputs=loss, inputs=adv_data_aug1)
            adv_data_aug1 = apply_grads(adv_data_aug1, data_aug1, grads1, adv_cfg)

        return adv_data_aug1.detach()


def byol_loss_function(x: torch.Tensor, y: torch.Tensor) -> torch.Tensor:
    x = normalize(x)
    y = normalize(y)
    return 2 - 2 * (x * y).sum(dim=-1).mean()
