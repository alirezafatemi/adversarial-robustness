import torch
from tqdm import tqdm

from src.adversary.adversary import apply_grads, get_grads, random_start
from src.configuration import (
    PretrainConfiguration,
    SimCLRConfiguration,
    SimCLRPGDAttackConfiguration,
)
from src.loss import adv_nt_xent, nt_xent
from src.methods.pretrain_base import PretrainBaseMethod
from src.model.model import SimCLR
from src.utils import AverageMeter


class SimCLRMethodPretrain(PretrainBaseMethod):
    model: SimCLR

    def __init__(self, cfg: PretrainConfiguration):
        assert isinstance(cfg.method_cfg, SimCLRConfiguration)
        super().__init__(cfg)

    def prepare_model(self):
        assert isinstance(self.cfg.method_cfg, SimCLRConfiguration)
        self.model = SimCLR(
            model=self.cfg.model,
            cifar_stem=self.cfg.cifar_stem,
            method_cfg=self.cfg.method_cfg,
            use_dual_bn=self.cfg.use_dual_bn,
        )

    def pretrain_step(self):
        assert isinstance(self.cfg.method_cfg, SimCLRConfiguration)
        loss_meter = AverageMeter()

        for data_aug1, data_aug2 in self.pretrain_loader:
            # Prepare inputs
            inputs = torch.cat((data_aug1, data_aug2))
            inputs = inputs.to(self.device, non_blocking=True)
            # Set model to train mode
            self.model.train()
            # Forward
            projections = self.model(inputs)
            # Calculate Loss
            loss = nt_xent(projections, self.cfg.method_cfg.temperature)
            # Optimize
            self.optimize(loss)

            loss_meter.update(loss.item())

        self.update_loss_stats(loss_meter.avg)

        self.log_images(data_aug1, "aug1")
        self.log_images(data_aug2, "aug2")

    def pretrain_adv_step(self):
        assert isinstance(self.cfg.method_cfg, SimCLRConfiguration)
        assert isinstance(self.cfg.adv_cfg, SimCLRPGDAttackConfiguration)
        adv_cfg = self.cfg.adv_cfg
        loss_meter = AverageMeter()
        reg_loss_meter = AverageMeter()

        for data_aug1, data_aug2 in self.pretrain_loader:
            data_aug1 = data_aug1.to(self.device)
            data_aug2 = data_aug2.to(self.device)
            # Generate Adversarial Sample
            if adv_cfg.attack_target == "self":
                data_target = data_aug1
            elif adv_cfg.attack_target == "other":
                data_target = data_aug2
            else:
                raise ValueError
            adv_data = self.attack(data_aug1, data_target)
            # Free GPU Space
            data_aug1 = data_aug1.to("cpu")
            data_aug2 = data_aug2.to("cpu")
            adv_data = adv_data.to("cpu")
            # Set model to train mode
            self.model.train()
            # Regularization
            if adv_cfg.regularize_to == "self":
                reg_target = data_aug1
            elif adv_cfg.regularize_to == "other":
                reg_target = data_aug2
            else:
                raise ValueError

            reg_loss = 0.0
            if adv_cfg.reg_weight > 0:
                if self.cfg.method_cfg.concat_inputs:
                    inputs = torch.cat((adv_data, reg_target))
                    inputs = inputs.to(self.device, non_blocking=True)
                    # Forward
                    projections = self.model(inputs)
                else:
                    projections_1 = self.model(
                        adv_data.to(self.device), is_natural=False
                    )
                    projections_2 = self.model(
                        reg_target.to(self.device), is_natural=True
                    )
                    projections = torch.cat((projections_1, projections_2))

                # Calculate Loss
                reg_loss = nt_xent(projections, self.cfg.method_cfg.temperature)

            xent_loss = 0.0
            if adv_cfg.xent_weight > 0:
                if self.cfg.method_cfg.concat_inputs:
                    # Prepare inputs
                    inputs = torch.cat((data_aug1, data_aug2, adv_data))
                    inputs = inputs.to(self.device, non_blocking=True)
                    # Forward
                    projections = self.model(inputs)
                else:
                    projections_1 = self.model(
                        data_aug1.to(self.device), is_natural=True
                    )
                    projections_2 = self.model(
                        data_aug2.to(self.device), is_natural=True
                    )
                    projections_3 = self.model(
                        adv_data.to(self.device), is_natural=False
                    )
                    projections = torch.cat(
                        (projections_1, projections_2, projections_3)
                    )
                xent_loss = adv_nt_xent(projections, self.cfg.method_cfg.temperature)
            # Calculate Loss
            loss = reg_loss * adv_cfg.reg_weight + xent_loss * adv_cfg.xent_weight
            # Optimize
            self.optimize(loss)

            loss_meter.update(loss.item())
            reg_loss_meter.update(reg_loss.item())

        self.update_loss_stats(loss_meter.avg)
        tqdm.write("reg loss: {}".format(reg_loss_meter.avg))
        self.log_images(data_aug1, "aug1")
        self.log_images(data_aug2, "aug2")
        self.log_images(adv_data, "adv")

    def attack(self, data_aug1: torch.Tensor, data_target: torch.Tensor):
        assert isinstance(self.cfg.method_cfg, SimCLRConfiguration)
        assert isinstance(self.cfg.adv_cfg, SimCLRPGDAttackConfiguration)

        adv_cfg = self.cfg.adv_cfg
        if adv_cfg.random_start:
            adv_data_aug1 = random_start(
                data_aug1,
                eps=adv_cfg.epsilon,
                norm=adv_cfg.attack_norm,
                min_val=adv_cfg.min_val,
                max_val=adv_cfg.max_val,
            )
        else:
            adv_data_aug1 = data_aug1

        # Set model to eval mode
        self.model.eval()
        if not self.cfg.method_cfg.concat_inputs:
            target_projections = self.model(data_target, is_natural=True)
        # Generate Adversarial Sample
        for _ in range(adv_cfg.iters):
            self.model.zero_grad()
            adv_data_aug1 = (
                adv_data_aug1.clone().detach().requires_grad_(True).to(self.device)
            )

            if self.cfg.method_cfg.concat_inputs:
                inputs = torch.cat((adv_data_aug1, data_target))
                projections = self.model(inputs)
            else:
                adv_projections = self.model(adv_data_aug1, is_natural=False)
                # Forward
                projections = torch.cat((adv_projections, target_projections))

            loss = nt_xent(projections, temperature=self.cfg.method_cfg.temperature)

            grads = get_grads(outputs=loss, inputs=adv_data_aug1)
            adv_data_aug1 = apply_grads(adv_data_aug1, data_aug1, grads, adv_cfg)

        return adv_data_aug1.detach()
