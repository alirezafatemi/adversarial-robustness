import os
from abc import ABC

import torch

from src.methods.base import BaseMethod


class TrainBaseMethod(BaseMethod, ABC):
    def initialize_stats(self) -> None:
        super(TrainBaseMethod, self).initialize_stats()
        self.stats.update({"best_val_acc": 0, "best_val_adv_acc": 0})

    def get_best_val_acc(self) -> float:
        return self.stats["best_val_acc"]

    def get_best_val_adv_acc(self) -> float:
        return self.stats["best_val_adv_acc"]

    def set_best_val_acc(self, new_best_val_acc) -> None:
        self.stats["best_val_acc"] = new_best_val_acc

    def set_best_val_adv_acc(self, new_best_val_adv_acc) -> None:
        self.stats["best_val_adv_acc"] = new_best_val_adv_acc

    def clear_stats(self) -> None:
        # Keep best_val_acc and best_val_adv_acc!
        best_val_acc = self.get_best_val_acc()
        best_val_adv_acc = self.get_best_val_adv_acc()
        self.stats = {}
        self.set_best_val_acc(best_val_acc)
        self.set_best_val_adv_acc(best_val_adv_acc)

    def save_checkpoint(
        self,
        is_best: bool = False,
        is_best_natural: bool = False,
        is_best_adversarial: bool = False,
    ):
        save_dir = self.model_save_dir
        epoch = self.current_epoch
        state = {
            "model": self.model.state_dict(),
            "optimizer": self.optimizer.state_dict(),
            "scheduler": self.lr_scheduler.state_dict(),
            "rng_state": torch.get_rng_state(),
            "epoch": epoch,
            "scaler": self.scaler.state_dict(),
            "best_val_acc": self.get_best_val_acc(),
            "best_val_adv_acc": self.get_best_val_adv_acc(),
        }
        if is_best:
            torch.save(state, os.path.join(save_dir, f"best_checkpoint.pt"))
            return
        elif is_best_natural:
            torch.save(state, os.path.join(save_dir, f"natural_best_checkpoint.pt"))
            return
        elif is_best_adversarial:
            torch.save(state, os.path.join(save_dir, f"adversarial_best_checkpoint.pt"))
            return
        else:
            torch.save(state, os.path.join(save_dir, f"checkpoint.pt"))
        if (epoch + 1) % self.cfg.save_frequency == 0:
            torch.save(state, os.path.join(save_dir, f"epoch_{epoch}.pt"))
        del state
        torch.cuda.empty_cache()

    def load_checkpoint(self, checkpoint_path: str):
        state = torch.load(checkpoint_path)
        self.model.load_state_dict(state["model"])
        self.optimizer.load_state_dict(state["optimizer"])
        self.lr_scheduler.load_state_dict(state["scheduler"])
        torch.set_rng_state(state["rng_state"])
        self.current_epoch = state["epoch"] + 1
        if "best_val_acc" in state:
            self.set_best_val_acc(state["best_val_acc"])
        if "best_val_adv_acc" in state:
            self.set_best_val_adv_acc(state["best_val_adv_acc"])
        if self.cfg.use_amp:
            self.scaler.load_state_dict(state["scaler"])
        del state
        torch.cuda.empty_cache()
