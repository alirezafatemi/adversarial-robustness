import os
from abc import ABC, abstractmethod

import torch
import torchvision
from torch import Tensor
from torch.cuda.amp import GradScaler
from torch.optim import Optimizer
from torch.optim.lr_scheduler import CosineAnnealingLR, LambdaLR, MultiStepLR
from torch.utils.data import DataLoader
from torch.utils.tensorboard import SummaryWriter
from tqdm import tqdm

from src.configuration import BaseConfiguration
from src.initialize import prepare_saving, prepare_writer
from src.model import BaseModel
from src.utils import Logger, select_device
from src.warmup_scheduler import GradualWarmupScheduler


class BaseMethod(ABC):
    cfg: BaseConfiguration
    optimizer: Optimizer
    lr_scheduler: torch.optim.lr_scheduler._LRScheduler
    train_loader: DataLoader
    test_loader: DataLoader
    writer: SummaryWriter
    model_save_dir: str
    device: str
    model: BaseModel
    stats: dict
    scaler: GradScaler

    def __init__(self, cfg: BaseConfiguration):
        self.cfg = cfg
        self.current_epoch = 0
        self.initialize_stats()
        self.writer = prepare_writer(
            base_dir=self.cfg.directories.base_dir, run_id=self.cfg.run_id
        )
        self.model_save_dir = prepare_saving(
            base_dir=self.cfg.directories.base_dir, run_id=self.cfg.run_id
        )
        self.device = select_device()

        self.prepare_model()
        self.relocate_model()
        self.prepare_data_loaders()
        self.prepare_optimizer()
        self.prepare_lr_scheduler()

        scaler = GradScaler(enabled=cfg.use_amp)
        self.scaler = scaler

        log_dir = os.path.join(self.cfg.directories.base_dir, self.cfg.run_id, "logs")
        self.logger = Logger(log_dir)

        if self.cfg.resume:
            assert self.cfg.directories.checkpoint_path is not None
            tqdm.write(f"Loading checkpoint...")
            self.load_checkpoint(
                checkpoint_path=self.cfg.directories.checkpoint_path,
            )
        else:
            self.writer.add_text("Configuration", str(self.cfg))
        tqdm.write(f"Run ID: {self.cfg.run_id}")

    @abstractmethod
    def prepare_data_loaders(self) -> None:
        pass

    @abstractmethod
    def prepare_model(self) -> None:
        pass

    @abstractmethod
    def relocate_model(self) -> None:
        pass

    @abstractmethod
    def prepare_optimizer(self) -> None:
        pass

    def initialize_stats(self):
        self.stats = {}

    def save_checkpoint(self):
        save_dir = self.model_save_dir
        epoch = self.current_epoch
        state = {
            "model": self.model.state_dict(),
            "optimizer": self.optimizer.state_dict(),
            "scheduler": self.lr_scheduler.state_dict(),
            "rng_state": torch.get_rng_state(),
            "epoch": epoch,
            "scaler": self.scaler.state_dict(),
        }
        torch.save(state, os.path.join(save_dir, f"checkpoint.pt"))
        if (epoch + 1) % self.cfg.save_frequency == 0:
            torch.save(state, os.path.join(save_dir, f"epoch_{epoch}.pt"))
        del state
        torch.cuda.empty_cache()

    def load_checkpoint(self, checkpoint_path: str):
        state = torch.load(checkpoint_path)
        self.model.load_state_dict(state["model"])
        self.optimizer.load_state_dict(state["optimizer"])
        self.lr_scheduler.load_state_dict(state["scheduler"])
        torch.set_rng_state(state["rng_state"])
        self.current_epoch = state["epoch"] + 1
        if self.cfg.use_amp:
            self.scaler.load_state_dict(state["scaler"])
        del state
        torch.cuda.empty_cache()

    def prepare_lr_scheduler(self) -> None:
        lr_scheduler_cfg = self.cfg.lr_scheduler_cfg
        lr_scheduler_name, epochs = (
            lr_scheduler_cfg.lr_scheduler,
            self.cfg.epochs,
        )
        if lr_scheduler_name == "cosine":
            if lr_scheduler_cfg.use_warmup:
                warmup_steps, lr_multiplier = (
                    lr_scheduler_cfg.warmup_steps,
                    lr_scheduler_cfg.lr_multiplier,
                )
                lr_scheduler = CosineAnnealingLR(
                    self.optimizer,
                    epochs - warmup_steps,
                    eta_min=lr_scheduler_cfg.min_lr,
                )
                lr_scheduler = GradualWarmupScheduler(
                    self.optimizer,
                    multiplier=lr_multiplier,
                    total_epoch=warmup_steps,
                    after_scheduler=lr_scheduler,
                )
            else:
                lr_scheduler = CosineAnnealingLR(
                    self.optimizer, epochs, eta_min=lr_scheduler_cfg.min_lr
                )
        elif lr_scheduler_name == "fixed":
            lr_scheduler = LambdaLR(self.optimizer, lr_lambda=lambda epoch: 1.0)
        elif lr_scheduler_name == "multi_step1":
            # From RoCL For Finetune. 150 Epochs
            lr_scheduler = MultiStepLR(
                self.optimizer, milestones=[30, 50, 100], gamma=0.1
            )
        elif lr_scheduler_name == "multi_step2":
            # From ACL For Training Before Pseudo Label Extraction.
            # 100 Epochs + Start LR = 0.1 + WD = 2e-4 + Batch Size = 128
            lr_scheduler = MultiStepLR(self.optimizer, milestones=[40, 60], gamma=0.1)
        elif lr_scheduler_name == "multi_step3":
            # From ACL For Semi-supervised Training
            # 15 Epochs + Start LR = 0.1 + WD = 5e-4 + Batch Size = 128
            lr_scheduler = MultiStepLR(self.optimizer, milestones=[5, 10], gamma=0.1)
        else:
            raise NotImplementedError(f"{lr_scheduler_name} scheduler not implemented!")
        self.lr_scheduler = lr_scheduler

    def log_stats(self) -> None:
        self.logger.info("Epoch {}:".format(self.current_epoch))
        for stat_name, stat_value in self.stats.items():
            log_msg = "{}: {:.6f}".format(stat_name, stat_value)
            tqdm.write(log_msg)
            self.writer.add_scalar(stat_name, stat_value, self.current_epoch)
            self.logger.info(log_msg)
        self.clear_stats()

    def clear_stats(self) -> None:
        self.stats = {}

    def log_images(self, images: Tensor, tag: str, num_images_to_log: int = 5) -> None:
        if not self.cfg.log_images:
            return
        if self.cfg.augmentation_cfg.normalize:
            raise NotImplementedError
        grid = torchvision.utils.make_grid(images[:num_images_to_log])
        self.writer.add_image(tag, grid, self.current_epoch)
