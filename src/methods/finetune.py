import time
from typing import Optional

import numpy as np
import torch
from torch.nn import KLDivLoss
from torch.nn.functional import cross_entropy, log_softmax, softmax
from torch.utils.data import DataLoader, SubsetRandomSampler
from torchvision.datasets import CIFAR10
from tqdm import tqdm

from src.adversary import pgd_attack_finetune, pgd_attack_trades
from src.augmentations import create_transforms
from src.configuration import FinetuneConfiguration
from src.data import cifar_basic_transforms
from src.data_loader import create_data_loaders_from_arrays
from src.methods.train_base import TrainBaseMethod
from src.model import FinetuneModel
from src.utils import (
    AverageMeter,
    accuracy,
    get_features,
    inference,
    select_device,
    tsne_visualize,
)

TRAIN_MODE = "train"
VAL_MODE = "val"
TEST_MODE = "test"


class Finetune(TrainBaseMethod):
    cfg: FinetuneConfiguration
    model: FinetuneModel
    val_loader: Optional[DataLoader]
    train_feature_loader: Optional[DataLoader]
    test_feature_loader: Optional[DataLoader]

    def __init__(self, cfg: FinetuneConfiguration):
        if cfg.visualize:
            # TODO: Not related to finetune
            self.cfg = cfg
            self.current_epoch = 0
            self.device = select_device()
            self.prepare_model()
            self.relocate_model()
            self.prepare_data_loaders()
        else:
            super(Finetune, self).__init__(cfg)

        self.train_feature_loader = None
        self.test_feature_loader = None
        if not cfg.encoder_trainable and cfg.use_frozen_features:
            self.prepare_feature_loaders()

    def prepare_model(self):
        if self.cfg.augmentation_cfg.dataset == "cifar-10":
            num_classes = 10
        elif self.cfg.augmentation_cfg.dataset == "cifar-100":
            num_classes = 100
        else:
            raise NotImplementedError
        self.model = FinetuneModel(
            model=self.cfg.model,
            cifar_stem=self.cfg.cifar_stem,
            num_classes=num_classes,
            use_dual_bn=self.cfg.use_dual_bn,
        )
        state = torch.load(self.cfg.directories.pretrain_checkpoint_path)
        self.model.load_state_dict(state["model"], strict=False)

    def relocate_model(self):
        self.model = self.model.to(self.device)

    def prepare_feature_loaders(self):
        tqdm.write("Computing features...")
        train_features, train_targets, test_features, test_targets = get_features(
            model=self.model.encoder,
            train_loader=self.train_loader,
            test_loader=self.test_loader,
            device=self.device,
        )
        tqdm.write("Features computed.")
        train_feature_loader, test_feature_loader = create_data_loaders_from_arrays(
            train_features,
            train_targets,
            test_features,
            test_targets,
            self.cfg.batch_size,
        )
        self.train_feature_loader = train_feature_loader
        self.test_feature_loader = test_feature_loader

    def prepare_data_loaders(self):
        train_transform = create_transforms(self.cfg.augmentation_cfg)
        test_transform = cifar_basic_transforms(self.cfg.augmentation_cfg)
        batch_size = self.cfg.batch_size

        test_dataset = CIFAR10(
            root=self.cfg.dataset_path,
            train=False,
            transform=test_transform,
            download=True,
        )
        test_loader = DataLoader(
            test_dataset,
            batch_size=batch_size,
            num_workers=4,
            pin_memory=False,
            shuffle=False,
        )
        self.test_loader = test_loader

        train_dataset = CIFAR10(
            root=self.cfg.dataset_path,
            train=True,
            transform=train_transform,
            download=True,
        )

        if self.cfg.percentage_labeled_data == 100 and not self.cfg.use_validation:
            # Use all training data
            train_loader = DataLoader(
                dataset=train_dataset,
                batch_size=batch_size,
                shuffle=True,
                drop_last=True,
                num_workers=4,
                pin_memory=True,
            )
            self.train_loader = train_loader
            self.val_loader = None
        else:
            self.cfg.use_validation = True
            if self.cfg.percentage_labeled_data == 100:
                train_idx_path = "src/split/train_idx_std.npy"
            elif self.cfg.percentage_labeled_data == 10:
                # 10% training labels
                train_idx_path = "src/split/train0.1_idx.npy"
            elif self.cfg.percentage_labeled_data == 1:
                # 1% training labels
                train_idx_path = "src/split/train0.01_idx.npy"
            else:
                raise NotImplementedError

            train_idx = list(np.load(train_idx_path))
            valid_idx = list(np.load("src/split/valid_idx_std.npy"))

            train_sampler = SubsetRandomSampler(train_idx)
            val_sampler = SubsetRandomSampler(valid_idx)

            val_dataset = CIFAR10(
                root=self.cfg.dataset_path,
                train=True,
                transform=test_transform,
                download=True,
            )

            train_loader = DataLoader(
                train_dataset, batch_size=batch_size, sampler=train_sampler
            )
            val_loader = DataLoader(
                val_dataset, batch_size=batch_size, sampler=val_sampler
            )

            self.train_loader = train_loader
            self.val_loader = val_loader

    def prepare_optimizer(self):
        optimizer_name, lr, weight_decay = (
            self.cfg.optimizer_cfg.optimizer,
            self.cfg.optimizer_cfg.lr,
            self.cfg.optimizer_cfg.weight_decay,
        )
        if optimizer_name == "sgd":
            optimizer = torch.optim.SGD(
                self.model.parameters(),
                lr=lr,
                momentum=self.cfg.optimizer_cfg.momentum,
                weight_decay=weight_decay,
                nesterov=self.cfg.optimizer_cfg.nesterov,
            )
        elif optimizer_name == "lars":
            from torchlars import LARS

            base_optimizer = torch.optim.SGD(
                self.model.parameters(),
                lr=lr,
                momentum=self.cfg.optimizer_cfg.momentum,
                weight_decay=weight_decay,
            )
            optimizer = LARS(base_optimizer)
        elif optimizer_name == "adam":
            optimizer = torch.optim.Adam(
                self.model.parameters(), lr=lr, weight_decay=weight_decay
            )
        else:
            raise NotImplementedError(f"{optimizer_name} optimizer not supported!")

        self.optimizer = optimizer

    def loss_function(self, x: torch.Tensor, y: torch.Tensor) -> torch.Tensor:
        return cross_entropy(x, y, reduction="mean")

    def get_proper_data_loader(self, mode: str) -> DataLoader:
        if mode == TRAIN_MODE:
            return self.train_loader
        elif mode == VAL_MODE:
            return self.val_loader
        elif mode == TEST_MODE:
            return self.test_loader

    def get_proper_feature_loader(self, mode: str) -> DataLoader:
        if mode == TRAIN_MODE:
            return self.train_feature_loader
        elif mode == TEST_MODE:
            return self.test_feature_loader
        elif mode == VAL_MODE:
            raise NotImplementedError

    def adapt_models(self, mode: str):
        if mode == TRAIN_MODE:
            if self.cfg.encoder_trainable:
                self.model.encoder.train()
            else:
                for param in self.model.encoder.parameters():
                    param.requires_grad = False
                self.model.encoder.eval()
            self.model.linear.train()
        else:
            self.model.encoder.eval()
            self.model.linear.eval()

    def finetune(self):
        start_epoch = self.current_epoch
        frozen = (
            not (
                self.cfg.adv_training
                or self.cfg.adv_statistics
                or self.cfg.encoder_trainable
            )
            and self.cfg.use_frozen_features
        )
        for current_epoch in tqdm(range(start_epoch, self.cfg.epochs)):
            self.current_epoch = current_epoch
            tqdm.write(f"\nEpoch {current_epoch}:")
            for mode in [TRAIN_MODE, VAL_MODE, TEST_MODE]:
                if (
                    mode == VAL_MODE
                    and self.val_loader is None
                    and not self.cfg.use_validation
                ):
                    continue

                tqdm.write("{} mode:".format(mode.capitalize()))

                start_time = time.time()
                if frozen:
                    self.finetune_frozen_step(mode=mode)
                else:
                    if self.cfg.trades and mode == TRAIN_MODE:
                        self.train_trades_step()
                    else:
                        self.finetune_step(mode=mode)

                end_time = time.time()

                self.log_stats()
                tqdm.write("{} Time: {:.4f}".format(mode, end_time - start_time))

                if mode == TRAIN_MODE:
                    self.lr_scheduler.step()

            self.save_checkpoint()

    def finetune_frozen_step(self, mode: str):
        loss_meter = AverageMeter()
        accuracy_meter = AverageMeter()

        self.adapt_models(mode=mode)

        data_loader = self.get_proper_feature_loader(mode=mode)
        for data, target in data_loader:
            data, target = (
                data.to(self.device),
                target.to(self.device),
            )

            logits = self.model.linear(data)
            loss = self.loss_function(logits, target)
            loss_meter.update(loss.item())

            if mode == TRAIN_MODE:
                self.optimizer.zero_grad()
                loss.backward()
                self.optimizer.step()

            acc = accuracy(logits.data, target)[0]
            accuracy_meter.update(acc.item(), data.shape[0])

        self.stats.update({"accuracy/{}".format(mode): accuracy_meter.avg})
        self.stats.update({"loss/{}".format(mode): loss_meter.avg})

        if mode == TRAIN_MODE:
            self.stats.update({"lr": self.lr_scheduler.get_last_lr()[0]})

    def finetune_step(self, mode: str):
        loss_meter = AverageMeter()
        accuracy_meter = AverageMeter()
        data_loader = self.get_proper_data_loader(mode=mode)

        adversary = self.cfg.adv_training or self.cfg.adv_statistics
        if adversary:
            adv_loss_meter = AverageMeter()
            adv_acc_meter = AverageMeter()

        for data, target in data_loader:
            target, data = (
                target.to(self.device),
                data.to(self.device),
            )

            if adversary:
                x_adv = pgd_attack_finetune(
                    encoder=self.model.encoder,
                    linear=self.model.linear,
                    images=data,
                    labels=target,
                    loss_fn=self.loss_function,
                    adv_cfg=self.cfg.adv_cfg,
                )
                self.adapt_models(mode=mode)
                adv_logits = self.model(x_adv)
                adv_loss = self.loss_function(adv_logits, target)
                adv_acc = accuracy(adv_logits.data, target)[0]
                adv_loss_meter.update(adv_loss.item())
                adv_acc_meter.update(adv_acc.item(), data.shape[0])

            self.adapt_models(mode=mode)

            if self.cfg.use_dual_bn and self.cfg.use_natural_bn:
                logits = self.model(data, is_natural=True)
            else:
                logits = self.model(data)
            loss = self.loss_function(logits, target)
            loss_meter.update(loss.item())

            if mode == TRAIN_MODE:
                self.optimizer.zero_grad()
                if self.cfg.adv_training:
                    adv_loss.backward()
                else:
                    loss.backward()
                self.optimizer.step()

            acc = accuracy(logits.data, target)[0]
            accuracy_meter.update(acc.item(), data.shape[0])

        if mode == VAL_MODE:
            if adversary:
                if accuracy_meter.avg > self.get_best_val_acc():
                    self.set_best_val_acc(accuracy_meter.avg)
                    self.save_checkpoint(is_best_natural=True)
                if adv_acc_meter.avg > self.get_best_val_adv_acc():
                    self.set_best_val_adv_acc(adv_acc_meter.avg)
                    self.save_checkpoint(is_best_adversarial=True)
            else:
                if accuracy_meter.avg > self.get_best_val_acc():
                    self.set_best_val_acc(accuracy_meter.avg)
                    self.save_checkpoint(is_best=True)

        self.stats.update({"accuracy/{}".format(mode): accuracy_meter.avg})
        self.stats.update({"loss/{}".format(mode): loss_meter.avg})

        if adversary:
            self.stats.update({"adv_accuracy/{}".format(mode): adv_acc_meter.avg})
            self.stats.update({"adv_loss/{}".format(mode): adv_loss_meter.avg})
            if mode == TRAIN_MODE and self.cfg.log_images:
                self.log_images(x_adv, "adv")

        if mode == TRAIN_MODE:
            self.stats.update({"lr": self.lr_scheduler.get_last_lr()[0]})
            if self.cfg.log_images:
                self.log_images(data, "natural")

    def train_trades_step(self):
        mode = TRAIN_MODE
        data_loader = self.train_loader

        loss_meter = AverageMeter()
        accuracy_meter = AverageMeter()
        adv_acc_meter = AverageMeter()

        criterion_kl = KLDivLoss(size_average=False)

        for data, target in data_loader:
            target, data = (
                target.to(self.device),
                data.to(self.device),
            )
            batch_size = len(data)

            x_adv = pgd_attack_trades(
                encoder=self.model.encoder,
                linear=self.model.linear,
                images=data,
                adv_cfg=self.cfg.adv_cfg,
            )

            self.adapt_models(mode=mode)

            adv_logits = self.model(x_adv)
            logits = self.model(data)

            loss = self.loss_function(logits, target)
            loss_robust = (1.0 / batch_size) * criterion_kl(
                log_softmax(adv_logits, dim=1), softmax(logits, dim=1)
            )
            loss += self.cfg.beta * loss_robust

            self.optimizer.zero_grad()
            loss.backward()
            self.optimizer.step()

            loss_meter.update(loss.item())
            acc = accuracy(logits.data, target)[0]
            accuracy_meter.update(acc.item(), data.shape[0])
            adv_acc = accuracy(adv_logits.data, target)[0]
            adv_acc_meter.update(adv_acc.item(), data.shape[0])

        self.stats.update({"accuracy/{}".format(mode): accuracy_meter.avg})
        self.stats.update({"loss/{}".format(mode): loss_meter.avg})
        self.stats.update({"adv_accuracy/{}".format(mode): adv_acc_meter.avg})
        self.stats.update({"lr": self.lr_scheduler.get_last_lr()[0]})

    # TODO: Not related to finetune
    def visualize(self, mode: str = "train"):
        print("Extracting features...")
        if mode == "train":
            features, targets = inference(self.train_loader, self.model, self.device)
        elif mode == "test":
            features, targets = inference(self.test_loader, self.model, self.device)
        else:
            print("Please choose between 'train' and 'test'")
            return
        print("features extracted.")
        start = time.time()
        print("start visualizing...")
        tsne_visualize(
            features=features,
            targets=targets,
            num_classes=10,  # for cifar-10
            visualization_dimension=self.cfg.visualization_dimension,
            fig_size=self.cfg.tsne_fig_size,
        )
        duration = time.time() - start
        print("visualization completed in {} seconds".format(duration))
