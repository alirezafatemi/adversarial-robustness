from typing import Tuple

import torch
from torch import Tensor
from torch.nn.functional import cosine_similarity

from src.adversary.adversary import apply_grads, get_grads, random_start
from src.configuration import PretrainConfiguration, SimSiamConfiguration
from src.loss import nt_xent
from src.methods.pretrain_base import PretrainBaseMethod
from src.model.model import SimSiam
from src.utils import AverageMeter


class SimSiamMethodPretrain(PretrainBaseMethod):
    model: SimSiam

    def __init__(self, cfg: PretrainConfiguration) -> None:
        assert isinstance(cfg.method_cfg, SimSiamConfiguration)
        super().__init__(cfg)

    def prepare_model(self) -> None:
        assert isinstance(self.cfg.method_cfg, SimSiamConfiguration)
        self.model = SimSiam(
            model=self.cfg.model,
            cifar_stem=self.cfg.cifar_stem,
            method_cfg=self.cfg.method_cfg,
            use_dual_bn=self.cfg.use_dual_bn,
        )

    def calculate_loss(
        self, predictions: Tuple[Tensor, ...], projections: Tuple[Tensor, ...]
    ) -> Tensor:
        loss, count = 0.0, 0
        for idx, prediction in enumerate(predictions):
            for other_projection in projections[:idx] + projections[idx + 1 :]:
                loss += simsiam_loss_function(prediction, other_projection)
                count += 1
        loss /= count
        return loss

    def pretrain_step(self) -> None:
        loss_meter = AverageMeter()

        for data_aug1, data_aug2 in self.pretrain_loader:
            data_aug1, data_aug2 = (
                data_aug1.to(self.device),
                data_aug2.to(self.device),
            )
            # Set model to train mode
            self.model.train()
            # Forward
            projection_1, prediction_1 = self.model(data_aug1)
            projection_2, prediction_2 = self.model(data_aug2)
            # Stop Gradient
            projection_1 = projection_1.detach()  # stop gradient
            projection_2 = projection_2.detach()  # stop gradient
            # Calculate Loss
            if self.cfg.method_cfg.use_contrastive_loss_in_train:
                loss = (
                    nt_xent(torch.cat((prediction_1, projection_2)))
                    + nt_xent(torch.cat((prediction_2, projection_1)))
                ) / 2
            else:
                loss = self.calculate_loss(
                    (prediction_1, prediction_2), (projection_1, projection_2)
                )
            self.optimize(loss)

            loss_meter.update(loss.item())

        self.update_loss_stats(loss_meter.avg)

    def pretrain_adv_step(self) -> None:
        assert self.cfg.adv_cfg is not None
        assert isinstance(self.cfg.method_cfg, SimSiamConfiguration)
        adv_cfg = self.cfg.adv_cfg
        method_cfg = self.cfg.method_cfg
        loss_meter = AverageMeter()

        for data_aug1, data_aug2 in self.pretrain_loader:
            data_aug1, data_aug2 = (
                data_aug1.to(self.device),
                data_aug2.to(self.device),
            )

            if adv_cfg.attack_to_branch == "both":
                adv_data_aug1_online, adv_data_aug2_target = self.attack_both(
                    online_data=data_aug1, target_data=data_aug2
                )
                # Flip
                adv_data_aug2_online, adv_data_aug1_target = self.attack_both(
                    online_data=data_aug2, target_data=data_aug1
                )
            elif adv_cfg.attack_to_branch == "online":
                adv_data_aug1_online = self.attack_online(
                    online_data=data_aug1, target_data=data_aug2
                )
                # Flip
                adv_data_aug2_online = self.attack_online(
                    online_data=data_aug2, target_data=data_aug1
                )
            elif adv_cfg.attack_to_branch == "dual_stream":
                # The loss is still symmetric.
                # Sum of one loss value calculated for perturbed samples and the other calculated for natural samples.
                adv_data_aug1_online, adv_data_aug2_target = self.attack_both(
                    online_data=data_aug1, target_data=data_aug2
                )
            else:
                raise ValueError

            self.model.train()

            if adv_cfg.attack_to_branch == "both":
                online_prediction_1 = self.model.extract_predictions(
                    adv_data_aug1_online, is_natural=False
                )
                online_prediction_2 = self.model.extract_predictions(
                    adv_data_aug2_online, is_natural=False
                )
                target_projection_1 = self.model.extract_projections(
                    adv_data_aug1_target, is_natural=False
                )
                target_projection_2 = self.model.extract_projections(
                    adv_data_aug2_target, is_natural=False
                )
            elif adv_cfg.attack_to_branch == "online":
                online_prediction_1 = self.model.extract_predictions(
                    adv_data_aug1_online, is_natural=False
                )
                online_prediction_2 = self.model.extract_predictions(
                    adv_data_aug2_online, is_natural=False
                )
                target_projection_1 = self.model.extract_projections(
                    data_aug1, is_natural=True
                )
                target_projection_2 = self.model.extract_projections(
                    data_aug2, is_natural=True
                )
            elif adv_cfg.attack_to_branch == "dual_stream":
                online_prediction_1 = self.model.extract_predictions(
                    adv_data_aug1_online, is_natural=False
                )
                online_prediction_2 = self.model.extract_predictions(
                    data_aug2, is_natural=True
                )
                target_projection_1 = self.model.extract_projections(
                    data_aug1, is_natural=True
                )
                target_projection_2 = self.model.extract_projections(
                    adv_data_aug2_target, is_natural=False
                )

            if method_cfg.detach_targets:
                target_projection_1 = target_projection_1.detach()  # stop gradient
                target_projection_2 = target_projection_2.detach()  # stop gradient

            if method_cfg.use_contrastive_loss_in_train:
                loss = (
                    nt_xent(torch.cat((online_prediction_1, target_projection_2)))
                    + nt_xent(torch.cat((online_prediction_2, target_projection_1)))
                ) / 2
            else:
                loss = self.calculate_loss(
                    (online_prediction_1, online_prediction_2),
                    (target_projection_1, target_projection_2),
                )

            self.optimize(loss)

            loss_meter.update(loss.item())

        self.update_loss_stats(loss_meter.avg)

    def attack_online(self, online_data: Tensor, target_data: Tensor) -> Tensor:
        adv_cfg = self.cfg.adv_cfg

        if adv_cfg.random_start:
            adv_data_online = random_start(
                online_data,
                eps=adv_cfg.epsilon,
                norm=adv_cfg.attack_norm,
                min_val=adv_cfg.min_val,
                max_val=adv_cfg.max_val,
            )
        else:
            adv_data_online = online_data

        # Set model to eval mode
        self.model.eval()
        # Extract projections for target
        target_projection = self.model.extract_projections(
            target_data, is_natural=True
        ).detach()
        for _ in range(adv_cfg.iters):
            self.model.zero_grad()
            adv_data_online = (
                adv_data_online.clone()
                .detach()
                .requires_grad_(True)
                .to(adv_data_online.device)
            )
            online_prediction = self.model.extract_predictions(
                adv_data_online, is_natural=False
            )
            if self.cfg.method_cfg.use_contrastive_loss_in_attack:
                loss = nt_xent(torch.cat((online_prediction, target_projection)))
            else:
                loss = simsiam_loss_function(
                    online_prediction, target_projection
                ).mean()

            grads_online = get_grads(outputs=loss, inputs=adv_data_online)
            adv_data_online = apply_grads(
                adv_data_online, online_data, grads_online, adv_cfg
            )

        return adv_data_online.detach()

    def attack_both(
        self, online_data: Tensor, target_data: Tensor
    ) -> Tuple[Tensor, Tensor]:
        adv_cfg = self.cfg.adv_cfg

        if adv_cfg.random_start:
            adv_data_online = random_start(
                online_data,
                eps=adv_cfg.epsilon,
                norm=adv_cfg.attack_norm,
                min_val=adv_cfg.min_val,
                max_val=adv_cfg.max_val,
            )
            adv_data_target = random_start(
                target_data,
                eps=adv_cfg.epsilon,
                norm=adv_cfg.attack_norm,
                min_val=adv_cfg.min_val,
                max_val=adv_cfg.max_val,
            )
        else:
            adv_data_online = online_data
            adv_data_target = target_data

        # Set model to eval mode
        self.model.eval()
        for _ in range(adv_cfg.iters):
            self.model.zero_grad()
            adv_data_online = (
                adv_data_online.clone()
                .detach()
                .requires_grad_(True)
                .to(adv_data_online.device)
            )
            adv_data_target = (
                adv_data_target.clone()
                .detach()
                .requires_grad_(True)
                .to(adv_data_target.device)
            )
            # Forward
            online_prediction = self.model.extract_predictions(
                adv_data_online, is_natural=False
            )
            target_projection = self.model.extract_projections(
                adv_data_target, is_natural=False
            )
            # Calculate Loss
            if self.cfg.method_cfg.use_contrastive_loss_in_attack:
                loss = nt_xent(torch.cat((online_prediction, target_projection)))
            else:
                loss = simsiam_loss_function(
                    online_prediction, target_projection
                ).mean()

            grads_online = get_grads(
                outputs=loss, inputs=adv_data_online, retain_graph=True
            )
            grads_target = get_grads(outputs=loss, inputs=adv_data_target)

            adv_data_online = apply_grads(
                adv_data_online, online_data, grads_online, adv_cfg
            )
            adv_data_target = apply_grads(
                adv_data_target, target_data, grads_target, adv_cfg
            )

        return adv_data_online.detach(), adv_data_target.detach()


def simsiam_loss_function(x: torch.Tensor, y: torch.Tensor) -> torch.Tensor:
    """
    Negative Cosine Similarity
    """
    return -cosine_similarity(x, y, dim=-1).mean()
