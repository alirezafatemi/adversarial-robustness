from typing import List, Tuple

import torch
from torch import Tensor

from src.adversary.adversary import apply_grads, get_grads, random_start
from src.configuration import (
    BYOLCLRConfiguration,
    AttackConfiguration,
    PretrainConfiguration,
)
from src.loss import nt_xent
from src.methods.pretrain_base import PretrainBaseMethod
from src.model import BYOLCLR
from src.utils import AverageMeter


class BYOLCLRMethodPretrain(PretrainBaseMethod):
    model: BYOLCLR

    def __init__(self, cfg: PretrainConfiguration) -> None:
        assert isinstance(cfg.method_cfg, BYOLCLRConfiguration)
        super().__init__(cfg)

    def prepare_model(self) -> None:
        assert isinstance(self.cfg.method_cfg, BYOLCLRConfiguration)
        self.model = BYOLCLR(
            model=self.cfg.model,
            cifar_stem=self.cfg.cifar_stem,
            method_cfg=self.cfg.method_cfg,
            use_dual_bn=self.cfg.use_dual_bn,
        )

    def update_moving_average_decay_stats(self, moving_average_decay: float) -> None:
        self.stats.update({"moving_average_decay": moving_average_decay})

    def extract_online_projections(self, views: Tuple[Tensor, ...]) -> List[Tensor]:
        online_projections = []
        for view in views:
            online_projection = self.model.forward_online(view)
            online_projections.append(online_projection)
        return online_projections

    def extract_target_projections(
        self, views: Tuple[Tensor, ...], detach_targets: bool = True
    ) -> List[Tensor]:
        target_projections = []
        for view in views:
            target_projection = self.model.forward_target(view)
            if detach_targets:
                target_projection = target_projection.detach()  # stop gradient
            target_projections.append(target_projection)
        return target_projections

    def pretrain_step(self):
        epoch = self.current_epoch
        loss_meter = AverageMeter()

        for data_aug1, data_aug2 in self.pretrain_loader:
            data_aug1, data_aug2 = (
                data_aug1.to(self.device),
                data_aug2.to(self.device),
            )

            self.model.train()
            views = (data_aug1, data_aug2)
            online_projections = self.extract_online_projections(views)
            target_projections = self.extract_target_projections(views)
            loss = nt_xent(torch.cat((online_projections, target_projections)))

            self.optimize(loss)

            self.model.update_moving_average(epoch=epoch, total_epochs=self.cfg.epochs)
            loss_meter.update(loss.item())

        self.update_loss_stats(loss_meter.avg)
        self.update_moving_average_decay_stats(self.model.current_moving_average_decay)

    def pretrain_adv_step(self) -> None:
        assert self.cfg.adv_cfg is not None
        assert isinstance(self.cfg.adv_cfg, AttackConfiguration)
        assert isinstance(self.cfg.method_cfg, BYOLCLRConfiguration)

        adv_cfg = self.cfg.adv_cfg

        epoch = self.current_epoch
        loss_meter = AverageMeter()

        for data_aug1, data_aug2 in self.pretrain_loader:
            data_aug1, data_aug2 = (
                data_aug1.to(self.device),
                data_aug2.to(self.device),
            )
            if adv_cfg.attack_to_branch == "online":
                adv_data_aug1 = self.attack_online(
                    online_data=data_aug1, target_data=data_aug2
                )
                # Flip
                adv_data_aug2 = self.attack_online(
                    online_data=data_aug2, target_data=data_aug1
                )

            self.model.train()

            if adv_cfg.attack_to_branch == "online":
                online_projections = self.extract_online_projections(
                    (adv_data_aug1, adv_data_aug2)
                )
                target_projections = self.extract_target_projections(
                    (data_aug1, data_aug2)
                )

            loss = (
                nt_xent(torch.cat((online_projections[0], target_projections[1])))
                + nt_xent(torch.cat((online_projections[1], target_projections[0])))
            ) / 2

            self.optimize(loss)

            self.model.update_moving_average(epoch=epoch, total_epochs=self.cfg.epochs)
            loss_meter.update(loss.item())

        self.update_loss_stats(loss_meter.avg)
        self.update_moving_average_decay_stats(self.model.current_moving_average_decay)

    def attack_online(self, online_data: Tensor, target_data: Tensor) -> Tensor:
        assert isinstance(self.cfg.method_cfg, BYOLCLRConfiguration)
        adv_cfg = self.cfg.adv_cfg

        if adv_cfg.random_start:
            adv_online_data = random_start(
                online_data,
                eps=adv_cfg.epsilon,
                norm=adv_cfg.attack_norm,
                min_val=adv_cfg.min_val,
                max_val=adv_cfg.max_val,
            )
        else:
            adv_online_data = online_data

        # Set model to eval mode
        self.model.eval()
        target_projection = self.extract_target_projections((target_data,))[0]
        # Generate Adversarial Sample
        for _ in range(adv_cfg.iters):
            self.model.zero_grad()
            adv_online_data = (
                adv_online_data.clone().detach().requires_grad_(True).to(self.device)
            )

            online_projection = self.extract_online_projections((adv_online_data,))[0]

            loss = nt_xent(torch.cat((online_projection, target_projection)))

            grads = get_grads(outputs=loss, inputs=adv_online_data)
            adv_online_data = apply_grads(adv_online_data, online_data, grads, adv_cfg)

        return adv_online_data.detach()
