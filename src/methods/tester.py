import torch
from autoattack import AutoAttack
from torch.nn.functional import cross_entropy
from torch.utils.data import DataLoader
from torchvision.datasets import CIFAR10
from tqdm import tqdm

from src.adversary import pgd_attack_finetune
from src.configuration import TesterConfiguration
from src.data import cifar_basic_transforms
from src.model import FinetuneModel
from src.utils import AverageMeter, accuracy, select_device


class Tester:
    cfg: TesterConfiguration
    model: FinetuneModel
    test_loader: DataLoader
    device: str

    def __init__(self, cfg: TesterConfiguration):
        self.cfg = cfg
        self.device = select_device()
        self.prepare_model()
        self.prepare_data_loaders()
        self.relocate_model()

    def prepare_model(self):
        if self.cfg.augmentation_cfg.dataset == "cifar-10":
            num_classes = 10
        elif self.cfg.augmentation_cfg.dataset == "cifar-100":
            num_classes = 100
        else:
            raise NotImplementedError
        self.model = FinetuneModel(
            model=self.cfg.model,
            cifar_stem=self.cfg.cifar_stem,
            num_classes=num_classes,
            use_dual_bn=self.cfg.use_dual_bn,
        )
        state = torch.load(self.cfg.checkpoint_path, map_location=self.device)
        self.model.load_state_dict(state["model"], strict=True)

    def relocate_model(self):
        self.model = self.model.to(self.device)

    def prepare_data_loaders(self):
        test_transform = cifar_basic_transforms(self.cfg.augmentation_cfg)
        batch_size = self.cfg.batch_size
        test_dataset = CIFAR10(
            root=self.cfg.dataset_path,
            train=False,
            transform=test_transform,
            download=True,
        )
        test_loader = DataLoader(
            dataset=test_dataset,
            batch_size=batch_size,
            shuffle=False,
            drop_last=False,
        )
        self.test_loader = test_loader

    def test(self) -> None:
        tqdm.write("Testing...")
        self.model.eval()
        acc_meter = AverageMeter()
        adv_acc_meter = AverageMeter()

        if self.cfg.adv_cfg.auto_attack:
            attack = AutoAttack(
                self.model,
                norm="Linf",
                eps=self.cfg.adv_cfg.epsilon,
                version="standard",
            )

        for data, target in tqdm(self.test_loader):
            target, data = (
                target.to(self.device),
                data.to(self.device),
            )
            logits = self.model(data)
            acc = accuracy(logits.data, target)[0]
            acc_meter.update(acc.item(), data.size(0))

            if self.cfg.adv_cfg.auto_attack:
                x_adv = attack.run_standard_evaluation(
                    data, target, bs=self.cfg.batch_size
                )
                # if self.cfg.log_images:
                #     self.log_images(data, "natural")
                #     self.log_images(x_adv, "adv")

            else:
                x_adv = pgd_attack_finetune(
                    encoder=self.model.encoder,
                    linear=self.model.linear,
                    images=data,
                    labels=target,
                    loss_fn=cross_entropy,
                    adv_cfg=self.cfg.adv_cfg,
                )
            adv_logits = self.model(x_adv)
            adv_acc = accuracy(adv_logits.data, target)[0]
            adv_acc_meter.update(adv_acc.item(), data.shape[0])

        tqdm.write("Natural Accuracy: {:.2f}".format(acc_meter.avg))
        tqdm.write("Adversarial Accuracy: {:.2f}".format(adv_acc_meter.avg))
