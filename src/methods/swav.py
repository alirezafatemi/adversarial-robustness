from typing import List, Tuple

import torch
import torch.distributed as dist
from torch import Tensor
from torch.nn.functional import softmax

from src.adversary.adversary import apply_grads, get_grads, random_start, unscale_grads
from src.configuration import (
    PretrainConfiguration,
    SwAVConfiguration,
    SwAVPGDAttackConfiguration,
)
from src.methods.pretrain_base import PretrainBaseMethod
from src.model.model import SwAV
from src.utils import AverageMeter


class SwAVMethodPretrain(PretrainBaseMethod):
    model: SwAV

    def __init__(self, cfg: PretrainConfiguration) -> None:
        assert isinstance(cfg.method_cfg, SwAVConfiguration)
        super().__init__(cfg)

    def prepare_model(self) -> None:
        assert isinstance(self.cfg.method_cfg, SwAVConfiguration)
        self.model = SwAV(
            model=self.cfg.model,
            cifar_stem=self.cfg.cifar_stem,
            method_cfg=self.cfg.method_cfg,
            l2norm=True,
            use_dual_bn=self.cfg.use_dual_bn,
        )

    def extract_outputs(
        self, views: Tuple[Tensor, ...], is_natural_stats: Tuple[bool, ...]
    ) -> List[Tensor]:
        self.model.normalize_prototypes()
        # Forward
        outputs = []
        for view, is_natural in zip(views, is_natural_stats):
            output = self.model(view, is_natural=is_natural)
            outputs.append(output)
        return outputs

    def calculate_assignments(
        self, outputs: List[Tensor], enable_grad: bool = False
    ) -> List[Tensor]:
        # Calculate assignments via sinkhorn
        assert isinstance(self.cfg.method_cfg, SwAVConfiguration)
        epsilon, sinkhorn_iterations = (
            self.cfg.method_cfg.epsilon,
            self.cfg.method_cfg.sinkhorn_iterations,
        )
        assignments = []
        with torch.set_grad_enabled(enable_grad):
            for output in outputs:
                q = torch.exp(output / epsilon).t()
                q = distributed_sinkhorn(q, sinkhorn_iterations)
                assignments.append(q)
        return assignments

    def calculate_predictions(self, outputs: List[Tensor]) -> List[Tensor]:
        # Cluster assignment prediction
        assert isinstance(self.cfg.method_cfg, SwAVConfiguration)
        temperature = self.cfg.method_cfg.temperature
        predictions = []
        for output in outputs:
            p = softmax(output / temperature, dim=1)
            predictions.append(p)
        return predictions

    def calculate_loss(
        self, predictions: List[Tensor], assignments: List[Tensor]
    ) -> Tensor:
        loss, count = 0.0, 0
        for idx, prediction in enumerate(predictions):
            # Swap Predictions
            for other_assignment in assignments[:idx] + assignments[idx + 1 :]:
                loss += swav_loss_function(prediction, other_assignment)
                count += 1
        loss /= count
        return loss

    def optimize(self, loss: Tensor, iteration: int) -> None:
        assert isinstance(self.cfg.method_cfg, SwAVConfiguration)
        freeze_prototypes_num_iters = self.cfg.method_cfg.freeze_prototypes_num_iters

        self.optimizer.zero_grad()
        # Scales the loss, and calls backward() to create scaled gradients
        self.scaler.scale(loss).backward()
        # Cancel some gradients if needed
        if iteration < freeze_prototypes_num_iters:
            for p in self.model.prototypes.parameters():
                p.grad = None
        # Unscales gradients and calls or skips optimizer.step()
        self.scaler.step(self.optimizer)
        # Updates the scale for next iteration.
        self.scaler.update()

    def pretrain_step(self) -> None:
        epoch = self.current_epoch
        loss_meter = AverageMeter()
        # One epoch
        for idx, (data_aug1, data_aug2) in enumerate(self.pretrain_loader):
            iteration = epoch * len(self.pretrain_loader) + idx
            # Prepare inputs
            data_aug1 = data_aug1.to(self.device)
            data_aug2 = data_aug2.to(self.device)
            # Switch to train mode
            self.model.train()
            # Put the views together
            views = (data_aug1, data_aug2)
            # Forward and calculate loss
            with torch.cuda.amp.autocast(enabled=self.cfg.use_amp):
                outputs = self.extract_outputs(views, (True, True))
                predictions = self.calculate_predictions(outputs)
                assignments = self.calculate_assignments(outputs)
                loss = self.calculate_loss(predictions, assignments)

            self.optimize(loss, iteration)

            loss_meter.update(loss.item())
        # End of epoch
        self.update_loss_stats(loss_meter.avg)

    def pretrain_adv_step(self) -> None:
        assert isinstance(self.cfg.adv_cfg, SwAVPGDAttackConfiguration)
        epoch = self.current_epoch
        loss_meter = AverageMeter()
        # One epoch
        for idx, (data_aug1, data_aug2) in enumerate(self.pretrain_loader):
            iteration = epoch * len(self.pretrain_loader) + idx
            original_data_aug1 = data_aug1.clone()
            original_data_aug2 = data_aug2.clone()
            # Prepare inputs
            data_aug1 = data_aug1.to(self.device)
            data_aug2 = data_aug2.to(self.device)
            if self.cfg.adv_cfg.attack_to_branch == "both":
                data_aug1, data_aug2 = self.attack_both(data_aug1, data_aug2)
                if self.cfg.adv_cfg.include_natural:
                    original_data_aug1 = original_data_aug1.to(self.device)
                    original_data_aug2 = original_data_aug2.to(self.device)
                    views = (
                        original_data_aug1,
                        original_data_aug2,
                        data_aug1,
                        data_aug2,
                    )
                    is_natural_stats = (True, True, False, False)
                else:
                    views = (data_aug1, data_aug2)
                    is_natural_stats = (False, False)
            else:
                data_aug1 = self.attack(data_aug1, data_aug2)
                if self.cfg.adv_cfg.include_natural:
                    original_data_aug1 = original_data_aug1.to(self.device)
                    views = (original_data_aug1, data_aug1, data_aug2)
                    is_natural_stats = (True, False, True)
                else:
                    views = (data_aug1, data_aug2)
                    is_natural_stats = (False, True)

            # Switch to train mode
            self.model.train()

            with torch.cuda.amp.autocast(enabled=self.cfg.use_amp):
                outputs = self.extract_outputs(views, is_natural_stats)
                predictions = self.calculate_predictions(outputs)
                assignments = self.calculate_assignments(outputs)
                loss = self.calculate_loss(predictions, assignments)

            self.optimize(loss, iteration)

            loss_meter.update(loss.item())
        # End of epoch
        self.update_loss_stats(loss_meter.avg)

        if self.cfg.adv_cfg.attack_to_branch == "both":
            self.log_images(data_aug1, "adv_data_aug1")
            self.log_images(data_aug2, "adv_data_aug2")
            self.log_images(original_data_aug1, "natural_data_aug1")
            self.log_images(original_data_aug2, "natural_data_aug2")
        else:
            self.log_images(data_aug1, "adv_data_aug1")
            self.log_images(original_data_aug1, "natural_data_aug1")
            self.log_images(original_data_aug2, "natural_data_aug2")

    def attack(self, data_aug1: Tensor, data_aug2: Tensor) -> Tensor:
        assert isinstance(self.cfg.adv_cfg, SwAVPGDAttackConfiguration)
        adv_cfg = self.cfg.adv_cfg

        if adv_cfg.random_start:
            adv_data_aug1 = random_start(
                data_aug1,
                eps=adv_cfg.epsilon,
                norm=adv_cfg.attack_norm,
                min_val=adv_cfg.min_val,
                max_val=adv_cfg.max_val,
            )
        else:
            adv_data_aug1 = data_aug1

        # Switch to eval mode
        self.model.eval()

        with torch.no_grad():
            with torch.cuda.amp.autocast(enabled=self.cfg.use_amp):
                outputs = self.extract_outputs((data_aug2,), is_natural_stats=(True,))
                q_2 = self.calculate_assignments(outputs)[0]
                if adv_cfg.include_natural_in_attack:
                    outputs = self.extract_outputs(
                        (data_aug1,), is_natural_stats=(True,)
                    )
                    q_1 = self.calculate_assignments(outputs)[0]

        # Generate Adversarial Sample
        for _ in range(adv_cfg.iters):
            self.model.zero_grad()
            self.model.eval()

            adv_data_aug1 = (
                adv_data_aug1.clone().detach().requires_grad_(True).to(self.device)
            )

            with torch.cuda.amp.autocast(enabled=self.cfg.use_amp):
                outputs = self.extract_outputs(
                    (adv_data_aug1,), is_natural_stats=(False,)
                )
                predictions = self.calculate_predictions(outputs)
                assignments = [None, q_2]
                if adv_cfg.include_natural_in_attack:
                    assignments.append(q_1)
                loss = self.calculate_loss(predictions, assignments)

            grads = get_grads(outputs=loss, inputs=adv_data_aug1, scaler=self.scaler)
            grads = unscale_grads(grads, self.scaler)
            adv_data_aug1 = apply_grads(adv_data_aug1, data_aug1, grads, adv_cfg)

        return adv_data_aug1.detach()

    def attack_both(
        self, data_aug1: Tensor, data_aug2: Tensor
    ) -> Tuple[Tensor, Tensor]:
        assert isinstance(self.cfg.adv_cfg, SwAVPGDAttackConfiguration)
        adv_cfg = self.cfg.adv_cfg

        if adv_cfg.random_start:
            adv_data1 = random_start(
                data_aug1,
                eps=adv_cfg.epsilon,
                norm=adv_cfg.attack_norm,
                min_val=adv_cfg.min_val,
                max_val=adv_cfg.max_val,
            )
            adv_data2 = random_start(
                data_aug2,
                eps=adv_cfg.epsilon,
                norm=adv_cfg.attack_norm,
                min_val=adv_cfg.min_val,
                max_val=adv_cfg.max_val,
            )
        else:
            adv_data1, adv_data2 = data_aug1, data_aug2

        # Switch to eval mode
        self.model.eval()

        if adv_cfg.include_natural_in_attack:
            with torch.no_grad():
                with torch.cuda.amp.autocast(enabled=self.cfg.use_amp):
                    outputs = self.extract_outputs(
                        (data_aug2, data_aug1), is_natural_stats=(True, True)
                    )
                    natural_assignments = self.calculate_assignments(outputs)
        # Generate Adversarial Sample
        for _ in range(adv_cfg.iters):
            self.model.zero_grad()
            self.model.eval()

            adv_data1 = adv_data1.clone().detach().requires_grad_(True).to(self.device)
            adv_data2 = adv_data2.clone().detach().requires_grad_(True).to(self.device)

            with torch.cuda.amp.autocast(enabled=self.cfg.use_amp):
                outputs = self.extract_outputs(
                    (adv_data1, adv_data2), is_natural_stats=(False, False)
                )
                predictions = self.calculate_predictions(outputs)
                assignments = self.calculate_assignments(outputs)
                if adv_cfg.include_natural_in_attack:
                    assignments += natural_assignments
                loss = self.calculate_loss(predictions, assignments)

            grads1 = get_grads(
                outputs=loss, inputs=adv_data1, retain_graph=True, scaler=self.scaler
            )  # retain_graph for grads2 calculation
            grads2 = get_grads(outputs=loss, inputs=adv_data2, scaler=self.scaler)

            grads1 = unscale_grads(grads1, self.scaler)
            grads2 = unscale_grads(grads2, self.scaler)

            adv_data1 = apply_grads(adv_data1, data_aug1, grads1, adv_cfg)
            adv_data2 = apply_grads(adv_data2, data_aug2, grads2, adv_cfg)

        return adv_data1.detach(), adv_data2.detach()


def distributed_sinkhorn(
    q: Tensor,
    nub_iters: int,
    world_size: int = 1,
    distributed: bool = False,
    enable_grad: bool = False,
) -> Tensor:
    with torch.set_grad_enabled(enable_grad):
        q = shoot_infs(q)
        sum_q = torch.sum(q)
        if distributed:
            dist.all_reduce(sum_q)
        q /= sum_q
        r = torch.ones(q.shape[0]).cuda(non_blocking=True) / q.shape[0]
        c = torch.ones(q.shape[1]).cuda(non_blocking=True) / (world_size * q.shape[1])
        for _ in range(nub_iters):
            u = torch.sum(q, dim=1)
            if distributed:
                dist.all_reduce(u)
            u = r / u
            u = shoot_infs(u)
            q *= u.unsqueeze(1)
            q *= (c / torch.sum(q, dim=0)).unsqueeze(0)
        return (q / torch.sum(q, dim=0, keepdim=True)).t().float()


def shoot_infs(inp_tensor):
    """Replaces inf by maximum of tensor"""
    mask_inf = torch.isinf(inp_tensor)
    ind_inf = torch.nonzero(mask_inf)
    if len(ind_inf) > 0:
        for ind in ind_inf:
            if len(ind) == 2:
                inp_tensor[ind[0], ind[1]] = 0
            elif len(ind) == 1:
                inp_tensor[ind[0]] = 0
        m = torch.max(inp_tensor)
        for ind in ind_inf:
            if len(ind) == 2:
                inp_tensor[ind[0], ind[1]] = m
            elif len(ind) == 1:
                inp_tensor[ind[0]] = m
    return inp_tensor


def swav_loss_function(prediction: Tensor, other_assignment: Tensor) -> Tensor:
    return -torch.mean(torch.sum(torch.log(prediction) * other_assignment, dim=1))
