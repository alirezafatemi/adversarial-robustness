import time
from typing import Tuple

import numpy as np
import torch
from torch import Tensor
from torch.nn import KLDivLoss, Module
from torch.nn.functional import cross_entropy, log_softmax, softmax
from torch.utils.data import DataLoader, SubsetRandomSampler
from torchvision.datasets import CIFAR10
from tqdm import tqdm

from src.adversary import pgd_attack_finetune
from src.augmentations import create_transforms
from src.configuration import SemiSupervisedConfiguration
from src.data import cifar_basic_transforms
from src.data.cifar import CIFAR10PseudoLabeled
from src.methods.train_base import TrainBaseMethod
from src.model import FinetuneModel
from src.utils import AverageMeter, accuracy

TRAIN_MODE = "train"
VAL_MODE = "val"
TEST_MODE = "test"


# Most of code from:
# https://github.com/VITA-Group/Adversarial-Contrastive-Learning/blob/master/train_trades_cifar10_semisupervised.py


class SemiSupervised(TrainBaseMethod):
    cfg: SemiSupervisedConfiguration
    labeled_loader: DataLoader
    pseudo_labeled_loader: DataLoader
    val_loader: DataLoader
    test_loader: DataLoader

    def __init__(self, cfg: SemiSupervisedConfiguration):
        super(SemiSupervised, self).__init__(cfg)
        self.initialize_stats()

    def prepare_model(self):
        if self.cfg.augmentation_cfg.dataset == "cifar-10":
            num_classes = 10
        elif self.cfg.augmentation_cfg.dataset == "cifar-100":
            num_classes = 100
        else:
            raise NotImplementedError
        self.model = FinetuneModel(
            model=self.cfg.model,
            cifar_stem=self.cfg.cifar_stem,
            num_classes=num_classes,
            use_dual_bn=self.cfg.use_dual_bn,
        )
        state = torch.load(self.cfg.directories.pretrain_checkpoint_path)
        self.model.load_state_dict(state["model"], strict=False)

    def relocate_model(self):
        self.model = self.model.to(self.device)

    def prepare_data_loaders(self):
        train_transform = create_transforms(self.cfg.augmentation_cfg)
        test_transform = cifar_basic_transforms(self.cfg.augmentation_cfg)
        batch_size = self.cfg.batch_size

        test_dataset = CIFAR10(
            root=self.cfg.dataset_path,
            train=False,
            transform=test_transform,
            download=True,
        )
        test_loader = DataLoader(
            test_dataset,
            batch_size=batch_size,
            num_workers=4,
            pin_memory=False,
            shuffle=False,
        )
        self.test_loader = test_loader

        val_dataset = CIFAR10(
            root=self.cfg.dataset_path,
            train=True,
            transform=test_transform,
            download=True,
        )
        pseudo_labels = np.load(self.cfg.pseudo_labels_path)

        train_datasets_pseudo_labeled = CIFAR10PseudoLabeled(
            pseudo_labels=pseudo_labels,
            root=self.cfg.dataset_path,
            train=True,
            download=True,
            transform=train_transform,
        )

        val_idx = list(np.load("src/split/valid_idx_std.npy"))
        if self.cfg.percentage_labeled_data == 10:
            label_idx = list(np.load("src/split/train0.1_idx.npy"))
            unlabel_idx = list(np.load("src/split/train0.9_unlabel_idx.npy"))
            batch_whole_num = 180
        elif self.cfg.percentage_labeled_data == 1:
            label_idx = list(np.load("src/split/train0.01_idx.npy"))
            unlabel_idx = list(np.load("src/split/train0.99_unlabel_idx.npy"))
            batch_whole_num = 225
        else:
            raise ValueError

        labeled_sampler = SubsetRandomSampler(label_idx)
        unlabeled_sampler = SubsetRandomSampler(unlabel_idx)
        val_sampler = SubsetRandomSampler(val_idx)

        labeled_loader = DataLoader(
            train_datasets_pseudo_labeled,
            batch_size=(len(label_idx) // batch_whole_num),
            shuffle=False,
            num_workers=4,
            sampler=labeled_sampler,
        )
        pseudo_labeled_loader = DataLoader(
            train_datasets_pseudo_labeled,
            batch_size=(len(unlabel_idx) // batch_whole_num),
            shuffle=False,
            num_workers=4,
            sampler=unlabeled_sampler,
        )

        val_loader = DataLoader(val_dataset, batch_size=batch_size, sampler=val_sampler)

        self.labeled_loader = labeled_loader
        self.pseudo_labeled_loader = pseudo_labeled_loader
        self.val_loader = val_loader

    def prepare_optimizer(self):
        optimizer_name, lr, weight_decay = (
            self.cfg.optimizer_cfg.optimizer,
            self.cfg.optimizer_cfg.lr,
            self.cfg.optimizer_cfg.weight_decay,
        )
        if optimizer_name == "sgd":
            optimizer = torch.optim.SGD(
                self.model.parameters(),
                lr=lr,
                momentum=self.cfg.optimizer_cfg.momentum,
                weight_decay=weight_decay,
                nesterov=self.cfg.optimizer_cfg.nesterov,
            )
        elif optimizer_name == "adam":
            optimizer = torch.optim.Adam(
                self.model.parameters(), lr=lr, weight_decay=weight_decay
            )
        else:
            raise NotImplementedError(f"{optimizer_name} optimizer not supported!")

        self.optimizer = optimizer

    def get_proper_data_loader(self, mode: str) -> DataLoader:
        if mode == VAL_MODE:
            return self.val_loader
        elif mode == TEST_MODE:
            return self.test_loader

    def run(self):
        start_epoch = self.current_epoch
        for current_epoch in tqdm(range(start_epoch, self.cfg.epochs)):
            self.current_epoch = current_epoch
            tqdm.write(f"\nEpoch {current_epoch}:")
            for mode in [TRAIN_MODE, VAL_MODE, TEST_MODE]:
                tqdm.write("{} mode:".format(mode.capitalize()))

                start_time = time.time()
                if mode == TRAIN_MODE:
                    self.train_epoch()
                    self.stats.update({"lr": self.lr_scheduler.get_last_lr()[0]})
                else:
                    self.evaluate(mode=mode)
                end_time = time.time()

                self.log_stats()
                tqdm.write("{} Time: {:.4f}".format(mode, end_time - start_time))

                if mode == TRAIN_MODE:
                    self.lr_scheduler.step()

            self.save_checkpoint()

    def train_epoch(self):
        loss_meter = AverageMeter()
        device = self.device
        pseudo_labeled_enum = enumerate(self.pseudo_labeled_loader)
        for batch_idx, (data, soft_target, target) in enumerate(self.labeled_loader):
            batch_idx_unlabeled, (inputs_unlabeled, pseudo_soft_target, _) = next(
                pseudo_labeled_enum
            )
            data, soft_target = torch.cat([data, inputs_unlabeled], dim=0), torch.cat(
                [soft_target, pseudo_soft_target], dim=0
            )
            data, soft_target, target = (
                data.to(device),
                soft_target.to(device),
                target.to(device),
            )

            # calculate robust loss
            loss, logits, adv_logits = self.calculate_trade_loss_soft(
                x_natural=data,
                y=target,
                y_soft=soft_target,
                beta=self.cfg.beta,
                alpha_coefficient=self.cfg.alpha_coefficient,
                temperature=self.cfg.temperature,
                rate_distill=self.cfg.rate_distill,
            )

            loss.backward()
            self.optimizer.step()

            loss_meter.update(loss.item())

        self.stats.update({"loss/train": loss_meter.avg})

    def evaluate(self, mode: str):
        loss_meter = AverageMeter()
        accuracy_meter = AverageMeter()
        adv_loss_meter = AverageMeter()
        adv_acc_meter = AverageMeter()

        data_loader = self.get_proper_data_loader(mode=mode)

        for data, target in data_loader:
            target, data = (
                target.to(self.device),
                data.to(self.device),
            )

            x_adv = pgd_attack_finetune(
                encoder=self.model.encoder,
                linear=self.model.linear,
                images=data,
                labels=target,
                loss_fn=cross_entropy,
                adv_cfg=self.cfg.adv_cfg,
            )
            self.model.eval()
            adv_logits = self.model(x_adv)
            adv_loss = cross_entropy(adv_logits, target)
            adv_acc = accuracy(adv_logits.data, target)[0]
            adv_loss_meter.update(adv_loss.item())
            adv_acc_meter.update(adv_acc.item(), data.shape[0])

            logits = self.model(data)
            loss = cross_entropy(logits, target)
            loss_meter.update(loss.item())

            acc = accuracy(logits.data, target)[0]
            accuracy_meter.update(acc.item(), data.shape[0])

        if mode == VAL_MODE:
            if accuracy_meter.avg > self.get_best_val_acc():
                self.set_best_val_acc(accuracy_meter.avg)
                self.save_checkpoint(is_best_natural=True)
            if adv_acc_meter.avg > self.get_best_val_adv_acc():
                self.set_best_val_adv_acc(adv_acc_meter.avg)
                self.save_checkpoint(is_best_adversarial=True)

        self.stats.update({"accuracy/{}".format(mode): accuracy_meter.avg})
        self.stats.update({"loss/{}".format(mode): loss_meter.avg})

        self.stats.update({"adv_accuracy/{}".format(mode): adv_acc_meter.avg})
        self.stats.update({"adv_loss/{}".format(mode): adv_loss_meter.avg})

    def calculate_trade_loss_soft(
        self,
        x_natural: Tensor,
        y: Tensor,
        y_soft: Tensor,
        beta: float = 1.0,
        temperature: float = 0,
        alpha_coefficient: float = 0,
        rate_distill: float = 1,
    ) -> Tuple[Tensor, Tensor, Tensor]:
        # Define KL-loss
        criterion_kl = KLDivLoss(size_average=False)
        criterion_distill = DistillCrossEntropy(temperature=temperature)
        batch_size = len(x_natural)
        # Generate adversarial example
        y_atk = torch.cat([y, y_soft[y.shape[0] :].max(1)[1]])
        x_adv = pgd_attack_finetune(
            encoder=self.model.encoder,
            linear=self.model.linear,
            images=x_natural,
            labels=y_atk,
            loss_fn=cross_entropy,
            adv_cfg=self.cfg.adv_cfg,
        )

        self.optimizer.zero_grad()
        self.model.train()

        logits = self.model(x_natural)
        logits_adv = self.model(x_adv)

        loss = (
            # Cross entropy for the labeled part
            cross_entropy(logits_adv[: y.shape[0]], y, reduction="sum")
            * alpha_coefficient
            / batch_size
            # Distill for the labeled part
            + criterion_distill(logits_adv[: y.shape[0]], y_soft[: y.shape[0]])
            * y_soft[: y.shape[0]].shape[0]
            * (temperature * temperature * rate_distill * (1.0 - alpha_coefficient))
            / batch_size
            # Distill for the unlabeled part
            + criterion_distill(logits_adv[y.shape[0] :], y_soft[y.shape[0] :])
            * y_soft[y.shape[0] :].shape[0]
            * (temperature * temperature * rate_distill)
            / batch_size
        )

        # Feature-consistency robustness loss for all data
        loss_robust = (1.0 / batch_size) * criterion_kl(
            log_softmax(logits_adv, dim=1), softmax(logits, dim=1)
        )
        loss += beta * loss_robust

        return loss, logits, logits_adv


class DistillCrossEntropy(Module):
    def __init__(self, temperature) -> None:
        super(DistillCrossEntropy, self).__init__()
        self.temperature = temperature

    def forward(self, inputs: Tensor, target: Tensor) -> Tensor:
        """
        :param inputs: prediction logits
        :param target: target logits
        :return: loss
        """
        log_likelihood = -log_softmax(inputs / self.temperature, dim=1)
        sample_num, class_num = target.shape
        loss = (
            torch.sum(
                torch.mul(
                    log_likelihood, torch.softmax(target / self.temperature, dim=1)
                )
            )
            / sample_num
        )

        return loss
