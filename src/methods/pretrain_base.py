import time
from abc import abstractmethod

import torch
from torch import Tensor
from torch.optim.lr_scheduler import LambdaLR
from torch.utils.data import DataLoader
from tqdm import tqdm

from src.augmentations import create_transforms
from src.configuration import PretrainConfiguration
from src.data import (
    cifar_basic_transforms,
    get_cifar100_datasets,
    get_cifar10_datasets,
    get_stl10_datasets,
    get_svhn_datasets,
    stl10_basic_transforms,
    svhn_basic_transforms,
)
from src.methods.base import BaseMethod
from src.utils import knn_monitor
from src.warmup_scheduler import GradualWarmupScheduler


class PretrainBaseMethod(BaseMethod):
    cfg: PretrainConfiguration
    pretrain_loader: DataLoader

    def __init__(self, cfg: PretrainConfiguration):
        super(PretrainBaseMethod, self).__init__(cfg)

    @abstractmethod
    def pretrain_step(self):
        raise NotImplementedError

    @abstractmethod
    def pretrain_adv_step(self):
        raise NotImplementedError

    def relocate_model(self):
        # from torch.nn.parallel import DistributedDataParallel
        # import torch.distributed as dist
        # gpu_count = torch.cuda.device_count()
        # dist.init_process_group("gloo", rank=0, world_size=gpu_count)
        self.model = self.model.to(self.device)
        # self.model = DistributedDataParallel(
        #     self.model,
        #     device_ids=[0],
        #     output_device=0
        # )

    def prepare_data_loaders(self):
        dataset = self.cfg.augmentation_cfg.dataset
        pretrain_transform = create_transforms(self.cfg.augmentation_cfg)
        if dataset == "cifar-10":
            basic_transform = cifar_basic_transforms(self.cfg.augmentation_cfg)
            pretrain_dataset, train_dataset, test_dataset = get_cifar10_datasets(
                pretrain_transform,
                basic_transform,
                basic_transform,
                root=self.cfg.dataset_path,
            )
        elif dataset == "cifar-100":
            basic_transform = cifar_basic_transforms(self.cfg.augmentation_cfg)
            pretrain_dataset, train_dataset, test_dataset = get_cifar100_datasets(
                pretrain_transform, basic_transform, basic_transform
            )
        elif dataset == "svhn":
            basic_transform = svhn_basic_transforms(self.cfg.augmentation_cfg)
            pretrain_dataset, train_dataset, test_dataset = get_svhn_datasets(
                pretrain_transform, basic_transform, basic_transform
            )
        elif dataset == "stl10":
            basic_transform = stl10_basic_transforms(self.cfg.augmentation_cfg)
            pretrain_dataset, train_dataset, test_dataset = get_stl10_datasets(
                pretrain_transform, basic_transform, basic_transform
            )
        else:
            raise NotImplementedError

        pretrain_loader = DataLoader(
            dataset=pretrain_dataset,
            batch_size=self.cfg.batch_size,
            shuffle=True,
            drop_last=True,
            num_workers=4,
            pin_memory=True,
        )
        train_loader = DataLoader(
            dataset=train_dataset,
            batch_size=self.cfg.batch_size,
            shuffle=False,
            drop_last=False,
            num_workers=4,
            pin_memory=True,
        )
        test_loader = DataLoader(
            dataset=test_dataset,
            batch_size=self.cfg.batch_size,
            shuffle=False,
            drop_last=False,
            num_workers=4,
            pin_memory=True,
        )

        self.pretrain_loader = pretrain_loader
        self.train_loader = train_loader
        self.test_loader = test_loader

    def prepare_optimizer(self):
        optimizer_name, lr, weight_decay = (
            self.cfg.optimizer_cfg.optimizer,
            self.cfg.optimizer_cfg.lr,
            self.cfg.optimizer_cfg.weight_decay,
        )
        params = self.model.parameters()
        if optimizer_name == "sgd":
            optimizer = torch.optim.SGD(
                params,
                lr=lr,
                momentum=self.cfg.optimizer_cfg.momentum,
                weight_decay=weight_decay,
            )
        elif optimizer_name == "lars":
            from torchlars import LARS

            base_optimizer = torch.optim.SGD(
                params,
                lr=lr,
                momentum=self.cfg.optimizer_cfg.momentum,
                weight_decay=weight_decay,
            )
            optimizer = LARS(base_optimizer)
        elif optimizer_name == "larc":
            from src.lars.larc import LARC

            base_optimizer = torch.optim.SGD(
                params,
                lr=lr,
                momentum=self.cfg.optimizer_cfg.momentum,
                weight_decay=weight_decay,
            )
            optimizer = LARC(
                optimizer=base_optimizer, trust_coefficient=0.001, clip=False
            )
        elif optimizer_name == "adam":
            optimizer = torch.optim.Adam(params, lr=lr, weight_decay=weight_decay)
        else:
            raise NotImplementedError(f"{optimizer_name} optimizer not supported!")

        self.optimizer = optimizer

    def prepare_lr_scheduler(self):
        lr_scheduler_cfg = self.cfg.lr_scheduler_cfg
        lr_scheduler_name, epochs = (
            lr_scheduler_cfg.lr_scheduler,
            self.cfg.epochs,
        )
        if lr_scheduler_name == "cosine":
            if lr_scheduler_cfg.use_warmup:
                warmup_steps, lr_multiplier = (
                    lr_scheduler_cfg.warmup_steps,
                    lr_scheduler_cfg.lr_multiplier,
                )
                lr_scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(
                    self.optimizer,
                    epochs - warmup_steps,
                    eta_min=lr_scheduler_cfg.min_lr,
                )
                lr_scheduler = GradualWarmupScheduler(
                    self.optimizer,
                    multiplier=lr_multiplier,
                    total_epoch=warmup_steps,
                    after_scheduler=lr_scheduler,
                )
            else:
                lr_scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(
                    self.optimizer, epochs, eta_min=lr_scheduler_cfg.min_lr
                )
        elif lr_scheduler_name == "fixed":
            lr_scheduler = LambdaLR(self.optimizer, lr_lambda=lambda epoch: 1.0)
        else:
            raise NotImplementedError(f"{lr_scheduler_name} optimizer not supported!")
        self.lr_scheduler = lr_scheduler

    def optimize(self, loss: Tensor) -> None:
        self.optimizer.zero_grad()
        # Scales the loss, and calls backward() to create scaled gradients
        self.scaler.scale(loss).backward()
        # Unscales gradients and calls or skips optimizer.step()
        self.scaler.step(self.optimizer)
        # Updates the scale for next iteration.
        self.scaler.update()

    def pretrain(self):
        start_epoch = self.current_epoch
        for current_epoch in tqdm(range(start_epoch, self.cfg.epochs)):
            # Update current epoch
            self.current_epoch = current_epoch
            tqdm.write(f"\nEpoch {current_epoch}:")

            start_time = time.time()

            if self.cfg.adv_training:
                self.pretrain_adv_step()
            else:
                self.pretrain_step()

            self.update_lr_stats()
            self.lr_scheduler.step()

            end_time = time.time()
            tqdm.write("Time: {}".format(end_time - start_time))

            if self.cfg.knn_statistics and (current_epoch + 1) % self.cfg.knn_freq == 0:
                accuracy = knn_monitor(
                    self.model.encoder,
                    self.train_loader,
                    self.test_loader,
                )
                self.update_accuracy_stats(accuracy)

            self.log_stats()
            self.save_checkpoint()

    def update_lr_stats(self) -> None:
        self.stats.update({"lr": self.lr_scheduler.get_last_lr()[0]})

    def update_accuracy_stats(self, accuracy: float) -> None:
        self.stats.update({"accuracy/test": accuracy})

    def update_loss_stats(self, avg_loss: float) -> None:
        self.stats.update({"loss/train": avg_loss})

    def update_sim_stats(
        self, avg_positive_sim: float, avg_negative_sim: float
    ) -> None:
        self.stats.update(
            {
                "positive_sim/train": avg_positive_sim,
                "negative_sim/train": avg_negative_sim,
            }
        )
