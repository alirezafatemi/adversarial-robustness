import torch
from torch.utils.data import DataLoader
from torchvision.datasets import CIFAR10
from tqdm import tqdm

from src.configuration import PseudoLabelConfiguration
from src.data import cifar_basic_transforms
from src.model import FinetuneModel
from src.utils import AverageMeter, accuracy, select_device


class PseudoLabel:
    cfg: PseudoLabelConfiguration
    model: FinetuneModel
    train_loader: DataLoader
    device: str

    def __init__(self, cfg: PseudoLabelConfiguration):
        self.cfg = cfg
        self.device = select_device()
        self.prepare_model()
        self.prepare_data_loaders()
        self.relocate_model()

    def prepare_model(self):
        if self.cfg.augmentation_cfg.dataset == "cifar-10":
            num_classes = 10
        elif self.cfg.augmentation_cfg.dataset == "cifar-100":
            num_classes = 100
        else:
            raise NotImplementedError
        self.model = FinetuneModel(
            model=self.cfg.model,
            cifar_stem=self.cfg.cifar_stem,
            num_classes=num_classes,
            use_dual_bn=self.cfg.use_dual_bn,
        )
        state = torch.load(self.cfg.checkpoint_path, map_location=self.device)
        self.model.load_state_dict(state["model"], strict=True)

    def relocate_model(self):
        self.model = self.model.to(self.device)

    def prepare_data_loaders(self):
        train_transform = cifar_basic_transforms(self.cfg.augmentation_cfg)
        batch_size = self.cfg.batch_size
        train_dataset = CIFAR10(
            root=self.cfg.dataset_path,
            train=True,
            transform=train_transform,
            download=True,
        )
        train_loader = DataLoader(
            dataset=train_dataset,
            batch_size=batch_size,
            shuffle=False,
            drop_last=False,
        )
        self.train_loader = train_loader

    def extract_pseudo_labels(self):
        tqdm.write("Extracting pseudo labels...")
        self.model.eval()
        acc_meter = AverageMeter()
        pseudo_labels = []
        for data, target in tqdm(self.train_loader):
            target, data = (
                target.to(self.device),
                data.to(self.device),
            )
            if self.cfg.use_dual_bn and self.cfg.use_natural_bn:
                logits = self.model(data, is_natural=True)
            else:
                logits = self.model(data)
            pseudo_labels += logits.cpu().detach().numpy().tolist()
            acc = accuracy(logits.data, target)[0]
            acc_meter.update(acc.item(), data.size(0))
        tqdm.write("Natural Accuracy: {:.2f}".format(acc_meter.avg))
        return pseudo_labels
