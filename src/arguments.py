from argparse import ArgumentParser


def add_basic_args(parser: ArgumentParser) -> ArgumentParser:
    parser.add_argument("--seed", default=42, type=int)
    parser.add_argument("--use_dual_bn", action="store_true")
    parser.add_argument(
        "--dataset",
        default="cifar-10",
        type=str,
        choices=["cifar-10", "cifar-100", "svhn", "stl10"],
    )
    parser.add_argument(
        "--model", default="resnet18", type=str, choices=["resnet18", "resnet50"]
    )
    parser.add_argument("--start_epoch", default=0, type=int)
    parser.add_argument("--epochs", default=1000, type=int)
    parser.add_argument("--save_frequency", default=10, type=int)
    parser.add_argument(
        "--base_dir", default="", type=str, help="Base directory for saving and logging"
    )
    # For resuming
    parser.add_argument(
        "--resume", "-r", action="store_true", help="Resume from checkpoint"
    )
    parser.add_argument("--checkpoint_path", type=str)
    parser.add_argument("--dataset_path", default="./data", type=str)

    return parser


def add_augmentations_args(parser: ArgumentParser) -> ArgumentParser:
    parser.add_argument("--image_size", default=32, type=int)
    parser.add_argument("--use_gaussian_blur", action="store_true")
    parser.add_argument("--use_color_jitter", action="store_true")
    parser.add_argument("--use_grayscale", action="store_true")
    parser.add_argument("--normalize", action="store_true")
    parser.add_argument("--use_random_resized_crop", action="store_true")
    parser.add_argument("--use_random_crop", action="store_true")
    parser.add_argument("--use_resize", action="store_true")
    parser.add_argument(
        "--color_jitter_strength", default=0.5, type=float, help="0.5 for CIFAR"
    )
    parser.add_argument(
        "--grayscale_prob", default=0.2, type=float, help="0.2 for CIFAR"
    )
    parser.add_argument(
        "--min_scale_crops", default=0.2, type=float, help="0.2 for CIFAR"
    )
    parser.add_argument(
        "--max_scale_crops", default=1.0, type=float, help="1.0 for CIFAR"
    )
    return parser


def add_optimization_args(parser: ArgumentParser) -> ArgumentParser:
    parser.add_argument(
        "--optimizer",
        default="lars",
        type=str,
        choices=["lars", "adam", "sgd", "larc"],
    )
    parser.add_argument("--batch_size", default=256, type=int)
    parser.add_argument("--momentum", default=0.9, type=float)
    parser.add_argument("--nesterov", default=True, type=bool)
    parser.add_argument("--weight_decay", default=1.5e-6, type=float)
    parser.add_argument(
        "--exclude_bias_parameters_from_lars", action="store_true", help="For LARS"
    )
    parser.add_argument(
        "--exclude_bn_parameters_from_lars", action="store_true", help="For LARS"
    )
    parser.add_argument("--lr", default=2e-1, type=float)
    parser.add_argument(
        "--lr_scheduler",
        default="cosine",
        type=str,
        choices=["cosine", "fixed", "multi_step1", "multi_step2", "multi_step3"],
    )
    parser.add_argument("--use_warmup", action="store_true")
    parser.add_argument("--lr_multiplier", default=10.0, type=float)
    parser.add_argument("--warmup_steps", default=10, type=int)
    parser.add_argument(
        "--min_lr", default=0.001, type=float, help="Min LR after decay"
    )
    # For pretraining, lr after warmup better be (0.3 * batch_size / 256)

    return parser


def add_byol_args(parser: ArgumentParser) -> ArgumentParser:
    parser.add_argument(
        "--moving_average_decay", default=0.996, type=float, help="For BYOL"
    )
    parser.add_argument(
        "--use_cosine_increasing_schedule",
        action="store_true",
        help="For BYOL",
    )
    parser.add_argument(
        "--use_contrastive_loss_in_train",
        action="store_true",
        help="For BYOL",
    )
    parser.add_argument(
        "--use_contrastive_loss_in_attack",
        action="store_true",
        help="For BYOL",
    )
    return parser


def add_swav_args(parser: ArgumentParser) -> ArgumentParser:
    parser.add_argument(
        "--num_prototypes", default=100, type=int, help="number of prototypes"
    )
    parser.add_argument(
        "--sinkhorn_iterations",
        default=3,
        type=int,
        help="number of iterations in Sinkhorn-Knopp algorithm",
    )
    parser.add_argument(
        "--swav_epsilon",
        default=0.05,
        type=float,
        help="regularization parameter for Sinkhorn-Knopp algorithm",
    )
    parser.add_argument(
        "--freeze_prototypes_num_iters",
        default=313,
        type=int,
        help="freeze the prototypes during this many iterations from the start",
    )

    return parser


def add_adversarial_args(parser: ArgumentParser) -> ArgumentParser:
    parser.add_argument(
        "--adv_training", action="store_true", help="Use Adversarial Training"
    )
    parser.add_argument(
        "--min_val", type=float, default=0.0, help="min for clipping image"
    )
    parser.add_argument(
        "--max_val", type=float, default=1.0, help="max for clipping image"
    )
    parser.add_argument(
        "--attack_norm",
        type=str,
        default="inf",
        choices=["inf", "l2", "l1"],
        help="Adversarial Attack Type",
    )
    parser.add_argument(
        "--epsilon",
        type=float,
        default=8 / 255,
        help="Maximum perturbation of adversaries (8/255=0.0314)",
    )
    parser.add_argument(
        "--alpha",
        type=float,
        default=2 / 255,
        help="Movement multiplier per iteration when generating adversarial examples (2/255=0.0078)",
    )
    parser.add_argument(
        "--attack_iters",
        type=int,
        default=10,
        help="Maximum iteration when generating adversarial examples",
    )
    parser.add_argument("--random_start", type=bool, default=True)
    parser.add_argument("--auto_attack", type=bool, default=False)

    parser.add_argument("--include_natural", action="store_true", help="For SwAV")
    parser.add_argument(
        "--include_natural_in_attack", action="store_true", help="For SwAV"
    )

    parser.add_argument(
        "--attack_to_branch",
        default="online",
        type=str,
        choices=["online", "target", "both", "dual_stream", "rocl", "rocl2"],
        help="For BYOL, SimSiam, and SwAV",
    )

    parser.add_argument(
        "--attack_target",
        default="other",
        type=str,
        choices=["self", "other"],
        help="For SimCLR",
    )
    parser.add_argument(
        "--regularize_to",
        default="other",
        type=str,
        choices=["self", "other"],
        help="For SimCLR",
    )
    parser.add_argument(
        "--reg_weight",
        type=float,
        default=1.0 / 256.0,
        help="Regularization weight introduced in RoCL",
    )
    parser.add_argument(
        "--xent_weight",
        type=float,
        default=1.0,
        help="Xent weight",
    )
    parser.add_argument("--concat_inputs", type=bool, default=True)
    return parser


def init_pretrain_argparse() -> ArgumentParser:
    parser = ArgumentParser(description="Adversarial BYOL Pretraining")

    parser.add_argument(
        "--method", type=str, choices=["byol", "simsiam", "swav", "simclr", "byolclr"]
    )
    parser.add_argument("--run_id", type=str, help="ID for this run")
    parser = add_basic_args(parser)
    parser = add_optimization_args(parser)

    parser.add_argument("--use_amp", action="store_true")
    # Architecture
    parser.add_argument("--projection_size", default=256, type=int)
    parser.add_argument("--projection_hidden_size", default=4096, type=int)
    parser.add_argument(
        "--projector_hidden_bn",
        action="store_true",
    )
    parser.add_argument(
        "--projector_out_bn",
        action="store_true",
    )
    parser.add_argument("--num_projection_layers", default=2, type=int)
    parser.add_argument("--prediction_hidden_size", default=512, type=int)
    parser.add_argument("--num_prediction_layers", default=2, type=int)
    parser.add_argument(
        "--predictor_hidden_bn",
        action="store_true",
    )
    parser.add_argument(
        "--predictor_out_bn",
        action="store_true",
    )

    parser.add_argument("--detach_targets", type=bool, default=True, help="For SimSiam")
    parser.add_argument(
        "--temperature",
        default=0.1,
        type=float,
        help="For SimCLR(0.5) & SwAV(0.1). temperature parameter in training loss",
    )

    parser.add_argument(
        "--knn_statistics", action="store_true", help="Print knn statistics"
    )
    parser.add_argument(
        "--knn_freq",
        default=10,
        type=int,
    )

    parser = add_swav_args(parser)
    parser = add_byol_args(parser)
    # Augmentations
    parser = add_augmentations_args(parser)
    # Adversarial Training
    parser = add_adversarial_args(parser)

    parser.add_argument(
        "--log_images",
        action="store_true",
        help="Add augmented images to tensorboard",
    )

    return parser


def init_finetune_argparse() -> ArgumentParser:
    parser = ArgumentParser(description="Finetune Pretrained Model")

    parser.add_argument("--run_id", type=str, help="ID for this run")
    parser.add_argument("--pretrain_id", type=str, help="ID for the pretrained model")
    parser.add_argument("--pretrain_checkpoint_path", type=str)

    parser = add_basic_args(parser)
    parser = add_optimization_args(parser)
    parser = add_augmentations_args(parser)
    parser = add_adversarial_args(parser)

    parser.add_argument(
        "--adv_statistics", action="store_true", help="Generate Adversarial Statistics"
    )

    parser.add_argument(
        "--encoder_trainable",
        action="store_true",
        help="Make encoder trainable in fine-tuning phase",
    )

    parser.add_argument(
        "--log_images",
        action="store_true",
        help="Add augmented images to tensorboard",
    )

    parser.add_argument("--use_amp", action="store_true")

    parser.add_argument(
        "--percentage_labeled_data",
        type=int,
        default=100,
        choices=[1, 10, 100],
        help="Percentage of labeled data, choose between [1, 10, 100]",
    )
    parser.add_argument("--use_validation", action="store_true")
    parser.add_argument("--use_frozen_features", type=bool, default=False)
    parser.add_argument("--use_natural_bn", type=bool, default=False)
    parser.add_argument("--trades", type=bool, default=False)
    parser.add_argument(
        "--beta",
        type=float,
        default=6.0,
        help="regularization, i.e., 1/lambda in TRADES",
    )
    # TODO: Not related to finetune
    parser.add_argument(
        "--visualize",
        action="store_true",
    )
    parser.add_argument(
        "--visualization_dimension",
        type=int,
        help="dimension of the visualized t-sne plot",
    )
    parser.add_argument(
        "--tsne_fig_size",
        nargs="+",
        type=int,
        help="figure size of the visualized t-sne plot",
    )

    return parser


def init_pseudo_label_argparse() -> ArgumentParser:
    parser = ArgumentParser(description="Extract pseudo labels")
    parser.add_argument("--seed", default=42, type=int)
    parser.add_argument("--checkpoint_path", type=str)
    parser.add_argument("--output_path", type=str)
    parser.add_argument("--batch_size", default=256, type=int)
    parser.add_argument(
        "--model", default="resnet18", type=str, choices=["resnet18", "resnet50"]
    )
    parser.add_argument(
        "--dataset",
        default="cifar-10",
        type=str,
        choices=["cifar-10", "cifar-100"],
    )
    parser.add_argument("--dataset_path", default="./data", type=str)
    parser.add_argument("--use_dual_bn", action="store_true")
    parser.add_argument("--use_natural_bn", type=bool, default=False)
    parser = add_augmentations_args(parser)
    return parser


def init_semi_supervised_argparse() -> ArgumentParser:
    parser = ArgumentParser(
        description="Semi-supervised training of the pretrained model"
    )
    parser.add_argument(
        "--alpha_coefficient", type=float, default=0.5, help="alpha coefficient in loss"
    )
    parser.add_argument(
        "--beta",
        type=float,
        default=6.0,
        help="regularization, i.e., 1/lambda in TRADES",
    )
    parser.add_argument(
        "--rate_distill", type=float, default=1.0, help="the rate for distillation loss"
    )
    parser.add_argument("--temperature", type=float, default=1.0)
    parser.add_argument(
        "--percentage_labeled_data",
        type=int,
        default=10,
        choices=[1, 10],
        help="Percentage of labeled data, choose between [1, 10]",
    )
    parser.add_argument("--run_id", type=str, help="ID for this run")
    parser.add_argument("--pretrain_checkpoint_path", type=str)
    parser.add_argument("--pseudo_labels_path", type=str)

    parser = add_basic_args(parser)
    parser = add_optimization_args(parser)
    parser = add_augmentations_args(parser)
    parser.add_argument(
        "--min_val", type=float, default=0.0, help="min for clipping image"
    )
    parser.add_argument(
        "--max_val", type=float, default=1.0, help="max for clipping image"
    )
    parser.add_argument(
        "--attack_norm",
        type=str,
        default="inf",
        choices=["inf", "l2", "l1"],
        help="Adversarial Attack Type",
    )
    parser.add_argument(
        "--epsilon",
        type=float,
        default=8 / 255,
        help="Maximum perturbation of adversaries (8/255=0.0314)",
    )
    parser.add_argument(
        "--alpha",
        type=float,
        default=2 / 255,
        help="Movement multiplier per iteration when generating adversarial examples (2/255=0.0078)",
    )
    parser.add_argument(
        "--attack_iters",
        type=int,
        default=10,
        help="Maximum iteration when generating adversarial examples",
    )
    parser.add_argument("--random_start", type=bool, default=True)
    parser.add_argument("--use_amp", action="store_true")

    return parser


def init_test_argparse() -> ArgumentParser:
    parser = ArgumentParser(description="Extract pseudo labels")
    parser.add_argument("--seed", default=42, type=int)
    parser.add_argument("--checkpoint_path", type=str)
    parser.add_argument("--batch_size", default=256, type=int)
    parser.add_argument(
        "--model", default="resnet18", type=str, choices=["resnet18", "resnet50"]
    )
    parser.add_argument(
        "--dataset",
        default="cifar-10",
        type=str,
        choices=["cifar-10", "cifar-100"],
    )
    parser.add_argument("--dataset_path", default="./data", type=str)
    parser.add_argument("--use_dual_bn", action="store_true")
    parser = add_augmentations_args(parser)
    parser = add_adversarial_args(parser)
    return parser
