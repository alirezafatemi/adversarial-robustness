from typing import Dict, Type, Union

import torch.multiprocessing as mp

from src.adversary.adversary import detect_norm
from src.arguments import init_pretrain_argparse
from src.configuration import (
    AttackConfiguration,
    AugmentationConfiguration,
    BYOLCLRConfiguration,
    BYOLConfiguration,
    DirectoryConfiguration,
    LRSchedulerConfiguration,
    OptimizerConfiguration,
    PretrainConfiguration,
    SimCLRConfiguration,
    SimCLRPGDAttackConfiguration,
    SimSiamConfiguration,
    SwAVConfiguration,
    SwAVPGDAttackConfiguration,
)
from src.initialize import prepare_cudnn, set_seed
from src.methods import (
    BYOLMethodPretrain,
    SimCLRMethodPretrain,
    SimSiamMethodPretrain,
    SwAVMethodPretrain,
)
from src.methods.byol_clr import BYOLCLRMethodPretrain

methods: Dict[
    str,
    Type[
        Union[
            SimSiamMethodPretrain,
            SimCLRMethodPretrain,
            SwAVMethodPretrain,
            BYOLMethodPretrain,
        ]
    ],
] = {
    "byol": BYOLMethodPretrain,
    "simclr": SimCLRMethodPretrain,
    "swav": SwAVMethodPretrain,
    "simsiam": SimSiamMethodPretrain,
    "byolclr": BYOLCLRMethodPretrain,
}


def main():
    mp.set_start_method("spawn")
    parser = init_pretrain_argparse()
    args = parser.parse_args()
    # Prepare Environment
    set_seed(seed=args.seed)
    prepare_cudnn(deterministic=True, benchmark=True)

    if args.method == "simsiam":
        method_cfg = SimSiamConfiguration(
            projection_size=args.projection_size,
            projection_hidden_size=args.projection_hidden_size,
            num_projection_layers=args.num_projection_layers,
            projector_hidden_bn=args.projector_hidden_bn,
            projector_out_bn=args.projector_out_bn,
            prediction_hidden_size=args.prediction_hidden_size,
            num_prediction_layers=args.num_prediction_layers,
            predictor_hidden_bn=args.predictor_hidden_bn,
            predictor_out_bn=args.predictor_out_bn,
            detach_targets=args.detach_targets,
            use_contrastive_loss_in_train=args.use_contrastive_loss_in_train,
            use_contrastive_loss_in_attack=args.use_contrastive_loss_in_attack,
        )
    elif args.method == "swav":
        method_cfg = SwAVConfiguration(
            projection_size=args.projection_size,
            projection_hidden_size=args.projection_hidden_size,
            num_projection_layers=args.num_projection_layers,
            projector_hidden_bn=args.projector_hidden_bn,
            projector_out_bn=args.projector_out_bn,
            freeze_prototypes_num_iters=args.freeze_prototypes_num_iters,
            num_prototypes=args.num_prototypes,
            epsilon=args.epsilon,
            sinkhorn_iterations=args.sinkhorn_iterations,
            temperature=args.temperature,
        )
    elif args.method == "byol":
        method_cfg = BYOLConfiguration(
            projection_size=args.projection_size,
            projection_hidden_size=args.projection_hidden_size,
            projector_hidden_bn=args.projector_hidden_bn,
            projector_out_bn=args.projector_out_bn,
            num_projection_layers=args.num_projection_layers,
            prediction_hidden_size=args.prediction_hidden_size,
            num_prediction_layers=args.num_prediction_layers,
            predictor_hidden_bn=args.predictor_hidden_bn,
            predictor_out_bn=args.predictor_out_bn,
            moving_average_decay=args.moving_average_decay,
            use_cosine_increasing_schedule=args.use_cosine_increasing_schedule,
            use_contrastive_loss_in_train=args.use_contrastive_loss_in_train,
            use_contrastive_loss_in_attack=args.use_contrastive_loss_in_attack,
        )
    elif args.method == "byolclr":
        method_cfg = BYOLCLRConfiguration(
            projection_size=args.projection_size,
            projection_hidden_size=args.projection_hidden_size,
            projector_hidden_bn=args.projector_hidden_bn,
            projector_out_bn=args.projector_out_bn,
            num_projection_layers=args.num_projection_layers,
            moving_average_decay=args.moving_average_decay,
            use_cosine_increasing_schedule=args.use_cosine_increasing_schedule,
        )
    elif args.method == "simclr":
        method_cfg = SimCLRConfiguration(
            projection_size=args.projection_size,
            projection_hidden_size=args.projection_hidden_size,
            projector_hidden_bn=args.projector_hidden_bn,
            projector_out_bn=args.projector_out_bn,
            num_projection_layers=args.num_projection_layers,
            temperature=args.temperature,
            concat_inputs=args.concat_inputs,
        )
    else:
        raise NotImplementedError
    directory_configuration = DirectoryConfiguration(
        base_dir=args.base_dir,
        checkpoint_path=args.checkpoint_path,
    )
    excluded_parameters = []
    if args.exclude_bias_parameters_from_lars:
        excluded_parameters.append(".bias")
    if args.exclude_bn_parameters_from_lars:
        excluded_parameters.append(".bn")
    optimizer_configuration = OptimizerConfiguration(
        optimizer=args.optimizer,
        weight_decay=args.weight_decay,
        lr=args.lr,
        momentum=args.momentum,
        nesterov=args.nesterov,
        excluded_parameters=excluded_parameters,
    )
    lr_scheduler_configuration = LRSchedulerConfiguration(
        lr_scheduler=args.lr_scheduler,
        use_warmup=args.use_warmup,
        lr_multiplier=args.lr_multiplier,
        warmup_steps=args.warmup_steps,
        min_lr=args.min_lr,
    )

    augmentation_configuration = AugmentationConfiguration(
        dataset=args.dataset,
        image_size=args.image_size,
        use_random_resized_crop=args.use_random_resized_crop,
        use_random_crop=args.use_random_crop,
        use_resize=args.use_resize,
        use_gaussian_blur=args.use_gaussian_blur,
        normalize=args.normalize,
        use_color_jitter=args.use_color_jitter,
        use_grayscale=args.use_grayscale,
        min_scale_crops=args.min_scale_crops,
        max_scale_crops=args.max_scale_crops,
        color_jitter_strength=args.color_jitter_strength,
        grayscale_prob=args.grayscale_prob,
    )

    adv_cfg = None
    if args.adv_training:
        attack_norm = detect_norm(args.attack_norm)
        if args.method == "simclr":
            adv_cfg = SimCLRPGDAttackConfiguration(
                attack_norm=attack_norm,
                random_start=args.random_start,
                iters=args.attack_iters,
                epsilon=args.epsilon,
                alpha=args.alpha,
                min_val=args.min_val,
                max_val=args.max_val,
                attack_target=args.attack_target,
                regularize_to=args.regularize_to,
                reg_weight=args.reg_weight,
                xent_weight=args.xent_weight,
                attack_to_branch=args.attack_to_branch,
            )
        elif args.method == "swav":
            adv_cfg = SwAVPGDAttackConfiguration(
                attack_norm=attack_norm,
                random_start=args.random_start,
                iters=args.attack_iters,
                epsilon=args.epsilon,
                alpha=args.alpha,
                min_val=args.min_val,
                max_val=args.max_val,
                attack_to_branch=args.attack_to_branch,
                include_natural=args.include_natural,
                include_natural_in_attack=args.include_natural_in_attack,
            )
        else:
            adv_cfg = AttackConfiguration(
                attack_norm=attack_norm,
                random_start=args.random_start,
                iters=args.attack_iters,
                epsilon=args.epsilon,
                alpha=args.alpha,
                min_val=args.min_val,
                max_val=args.max_val,
                attack_to_branch=args.attack_to_branch,
            )

    configuration = PretrainConfiguration(
        seed=args.seed,
        use_dual_bn=args.use_dual_bn,
        batch_size=args.batch_size,
        model=args.model,
        cifar_stem=True,
        dataset_path=args.dataset_path,
        epochs=args.epochs,
        resume=args.resume,
        run_id=args.run_id,
        log_images=args.log_images,
        save_frequency=args.save_frequency,
        directories=directory_configuration,
        optimizer_cfg=optimizer_configuration,
        lr_scheduler_cfg=lr_scheduler_configuration,
        augmentation_cfg=augmentation_configuration,
        adv_training=args.adv_training,
        adv_cfg=adv_cfg,
        method=args.method,
        method_cfg=method_cfg,
        knn_statistics=args.knn_statistics,
        knn_freq=args.knn_freq,
        use_amp=args.use_amp,
    )

    method = methods[args.method](cfg=configuration)
    method.pretrain()


if __name__ == "__main__":
    main()
