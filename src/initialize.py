import os
import random

import numpy as np
import torch
from torch.backends import cudnn
from torch.utils.tensorboard import SummaryWriter


def set_seed(seed: int = 42) -> None:
    torch.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)
    random.seed(seed)
    np.random.seed(seed)


def prepare_cudnn(deterministic: bool = None, benchmark: bool = None) -> None:
    if torch.cuda.is_available():
        if deterministic is None:
            deterministic = os.environ.get("CUDNN_DETERMINISTIC", "True") == "True"
        if benchmark is None:
            benchmark = os.environ.get("CUDNN_BENCHMARK", "True") == "True"
        cudnn.deterministic = deterministic
        cudnn.benchmark = benchmark


def prepare_writer(base_dir: str, run_id: str) -> SummaryWriter:
    log_dir = os.path.join(base_dir, run_id, "logs")

    os.makedirs(log_dir, exist_ok=True)

    writer = SummaryWriter(log_dir)
    return writer


def prepare_saving(base_dir: str, run_id: str) -> str:
    model_save_dir = os.path.join(base_dir, run_id, "models")
    os.makedirs(model_save_dir, exist_ok=True)
    return model_save_dir
