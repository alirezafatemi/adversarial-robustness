from typing import Callable, Optional, Union

import torch
from torch import Tensor
from torch.nn.functional import log_softmax, softmax
from torch.cuda.amp import GradScaler
from torch.nn import Module

from src.configuration import FinetuneAttackConfiguration, AttackConfiguration


# Later have a look at the attacks in:
# https://github.com/tml-epfl/understanding-fast-adv-training/blob/master/utils.py
# https://github.com/Harry24k/adversarial-attacks-pytorch/blob/master/torchattacks/attacks/pgd.py
# https://github.com/Harry24k/adversarial-attacks-pytorch/blob/master/torchattacks/attacks/pgdl2.py


def project(
    natural_images: Tensor, adv_images: Tensor, norm: Union[str, int], epsilon: float
) -> Tensor:
    """
    Projects x_adv into the l_norm ball around x.
    Assumes x and x_adv are 4D Tensors representing batches of images.

    Args:
        natural_images: Batch of natural images
        adv_images: Batch of adversarial images
        norm: Norm of ball around x
        epsilon: Radius of ball

    Returns:
        x_adv: Adversarial examples projected to be at most eps
            distance from x under a certain norm
    """
    if natural_images.shape != adv_images.shape:
        raise ValueError("Input Tensors must have the same shape")

    if norm == "inf":
        # Workaround as PyTorch doesn't have elementwise clip
        adv_images = torch.max(
            torch.min(adv_images, natural_images + epsilon), natural_images - epsilon
        )
    else:
        delta = adv_images - natural_images

        # Assume x and x_adv are batched tensors where the first dimension is a batch dimension
        mask = delta.view(delta.shape[0], -1).norm(norm, dim=1) <= epsilon

        scaling_factor = delta.view(delta.shape[0], -1).norm(norm, dim=1)
        scaling_factor[mask] = epsilon

        # .view() assumes batched images as a 4D Tensor
        delta *= epsilon / scaling_factor.view(-1, 1, 1, 1)

        adv_images = natural_images + delta

    return adv_images


def detect_norm(attack_norm: str) -> Union[str, int]:
    if attack_norm == "l1":
        return 1
    elif attack_norm == "l2":
        return 2
    return attack_norm


def pgd_attack_finetune(
    encoder: Module,
    linear: Module,
    images: Tensor,
    labels: Tensor,
    loss_fn: Callable[[Tensor, Tensor], Tensor],
    adv_cfg: FinetuneAttackConfiguration,
) -> torch.Tensor:
    encoder = encoder.eval()
    linear = linear.eval()

    adv_images = images.clone().detach().requires_grad_(True).to(images.device)
    if adv_cfg.random_start:
        adv_images = random_start(
            adv_images,
            adv_cfg.epsilon,
            adv_cfg.attack_norm,
            adv_cfg.min_val,
            adv_cfg.max_val,
        )

    for _ in range(adv_cfg.iters):
        adv_images = adv_images.clone().detach().requires_grad_(True)

        outputs = encoder(adv_images)
        outputs = linear(outputs.squeeze())

        encoder.zero_grad()
        linear.zero_grad()
        loss = loss_fn(outputs, labels)

        grads = get_grads(outputs=loss, inputs=adv_images)
        adv_images = apply_grads(adv_images, images, grads, adv_cfg)

    return adv_images.detach()


def pgd_attack_trades(
    encoder: Module,
    linear: Module,
    images: Tensor,
    adv_cfg: FinetuneAttackConfiguration,
) -> torch.Tensor:
    encoder = encoder.eval()
    linear = linear.eval()
    criterion_kl = torch.nn.KLDivLoss(size_average=False)

    adv_images = images.clone().detach().requires_grad_(True).to(images.device)
    if adv_cfg.random_start:
        adv_images = random_start(
            adv_images,
            adv_cfg.epsilon,
            adv_cfg.attack_norm,
            adv_cfg.min_val,
            adv_cfg.max_val,
        )
    natural_outputs = linear(encoder(images).squeeze()).detach()
    for _ in range(adv_cfg.iters):
        adv_images = adv_images.clone().detach().requires_grad_(True)

        adv_outputs = linear(encoder(adv_images).squeeze())

        encoder.zero_grad()
        linear.zero_grad()
        loss = criterion_kl(
            log_softmax(adv_outputs, dim=1), softmax(natural_outputs, dim=1)
        )

        grads = get_grads(outputs=loss, inputs=adv_images)
        adv_images = apply_grads(adv_images, images, grads, adv_cfg)

    return adv_images.detach()


def random_start(
    adv_images: Tensor,
    eps: float,
    norm: Union[str, int],
    min_val: float,
    max_val: float,
) -> Tensor:
    if norm == "inf":
        adv_images = adv_images + torch.empty_like(adv_images).uniform_(-eps, eps)
        adv_images = torch.clamp(adv_images, min=min_val, max=max_val).detach()
    else:
        # https://github.com/BorealisAI/advertorch/blob/master/advertorch/attacks/utils.py
        raise NotImplementedError
    return adv_images


@torch.no_grad()
def apply_grads(
    adv_data: Tensor,
    natural_data: Tensor,
    grads: Tensor,
    adv_cfg: Union[AttackConfiguration, FinetuneAttackConfiguration],
    eps_for_division: float = 1e-10,
):
    if adv_cfg.attack_norm == "inf":
        grads = grads.sign()
    else:
        # .view() assumes batched image data as 4D tensor
        grads = grads / (
            grads.view(grads.shape[0], -1).norm(adv_cfg, dim=-1).view(-1, 1, 1, 1)
            + eps_for_division
        )
    grads = grads * adv_cfg.alpha
    adv_data += grads
    adv_data = project(
        natural_data,
        adv_data,
        adv_cfg.attack_norm,
        adv_cfg.epsilon,
    ).clamp(min=adv_cfg.min_val, max=adv_cfg.max_val)

    return adv_data


def get_grads(
    outputs: Tensor,
    inputs: Tensor,
    retain_graph: bool = False,
    scaler: Optional[GradScaler] = None,
) -> Tensor:
    if scaler is not None:
        return torch.autograd.grad(
            outputs=scaler.scale(outputs),
            inputs=inputs,
            only_inputs=True,
            retain_graph=retain_graph,
            create_graph=False,
        )[0]
    return torch.autograd.grad(
        outputs=outputs,
        inputs=inputs,
        only_inputs=True,
        retain_graph=retain_graph,
        create_graph=False,
    )[0]


def unscale_grads(scaled_grad_param, scaler: GradScaler):
    # inv_scale will be one if use_amp==False
    inv_scale = 1.0 / scaler.get_scale()
    grad_params = scaled_grad_param * inv_scale
    return grad_params
