from src.arguments import init_test_argparse
from src.configuration import (
    AugmentationConfiguration,
    FinetuneAttackConfiguration,
    TesterConfiguration,
)
from src.initialize import prepare_cudnn, set_seed
from src.methods import Tester


def main():
    parser = init_test_argparse()
    args = parser.parse_args()
    # Prepare Environment
    set_seed(seed=args.seed)
    prepare_cudnn(deterministic=True, benchmark=True)

    augmentation_configuration = AugmentationConfiguration(
        dataset=args.dataset,
        image_size=args.image_size,
        use_random_resized_crop=args.use_random_resized_crop,
        use_random_crop=args.use_random_crop,
        use_resize=args.use_resize,
        use_gaussian_blur=args.use_gaussian_blur,
        normalize=args.normalize,
        use_color_jitter=args.use_color_jitter,
        use_grayscale=args.use_grayscale,
        min_scale_crops=args.min_scale_crops,
        max_scale_crops=args.max_scale_crops,
        color_jitter_strength=args.color_jitter_strength,
        grayscale_prob=args.grayscale_prob,
    )
    adv_configuration = FinetuneAttackConfiguration(
        attack_norm=args.attack_norm,
        random_start=args.random_start,
        iters=args.attack_iters,
        epsilon=args.epsilon,
        alpha=args.alpha,
        min_val=args.min_val,
        max_val=args.max_val,
        auto_attack=args.auto_attack,
    )
    configuration = TesterConfiguration(
        seed=args.seed,
        model=args.model,
        cifar_stem=True,
        checkpoint_path=args.checkpoint_path,
        batch_size=args.batch_size,
        augmentation_cfg=augmentation_configuration,
        dataset_path=args.dataset_path,
        use_dual_bn=args.use_dual_bn,
        adv_cfg=adv_configuration,
        normalize=args.normalize,
    )

    tester = Tester(cfg=configuration)
    tester.test()


if __name__ == "__main__":
    main()
