import numpy as np
import torch
from torch.utils.data import DataLoader


def create_data_loaders_from_arrays(
    train_features: np.array,
    train_targets: np.array,
    test_features: np.array,
    test_targets: np.array,
    batch_size: int,
):
    train_dataset = torch.utils.data.TensorDataset(
        torch.from_numpy(train_features), torch.from_numpy(train_targets)
    )
    train_loader = torch.utils.data.DataLoader(
        train_dataset, batch_size=batch_size, shuffle=True
    )

    test_dataset = torch.utils.data.TensorDataset(
        torch.from_numpy(test_features),
        torch.from_numpy(test_targets),
    )
    test_loader = torch.utils.data.DataLoader(
        test_dataset, batch_size=batch_size, shuffle=False
    )
    return train_loader, test_loader
