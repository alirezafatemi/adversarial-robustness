from PIL import Image
from torchvision import transforms

from src.configuration import AugmentationConfiguration

# mean and std from https://inferno-pytorch.readthedocs.io/en/latest/_modules/inferno/io/box/cifar.html
CIFAR_10_MEAN = (0.4914, 0.4822, 0.4465)
CIFAR_10_STD = (0.247, 0.2435, 0.2616)
CIFAR_100_MEAN = (0.5071, 0.4865, 0.4409)
CIFAR_100_STD = (0.2673, 0.2564, 0.2762)


def create_transforms(
    augmentation_cfg: AugmentationConfiguration,
) -> transforms.Compose:
    transforms_list = []

    if augmentation_cfg.use_random_resized_crop:
        transforms_list.append(
            transforms.RandomResizedCrop(
                size=augmentation_cfg.image_size,
                scale=(
                    augmentation_cfg.min_scale_crops,
                    augmentation_cfg.max_scale_crops,
                ),
                interpolation=Image.BICUBIC,  # 3
            )
        )
    elif augmentation_cfg.use_random_crop:
        transforms_list.append(
            transforms.RandomCrop(size=augmentation_cfg.image_size, padding=4)
        )

    if augmentation_cfg.use_resize:
        transforms_list.append(transforms.Resize(size=augmentation_cfg.image_size))

    if augmentation_cfg.use_random_horizontal_flip:
        transforms_list.append(transforms.RandomHorizontalFlip(p=0.5))

    if augmentation_cfg.use_color_jitter:
        rnd_color_jitter = get_color_jitter(augmentation_cfg.color_jitter_strength)
        transforms_list.append(rnd_color_jitter)

    if augmentation_cfg.use_grayscale:
        rnd_gray = transforms.RandomGrayscale(p=augmentation_cfg.grayscale_prob)
        transforms_list.append(rnd_gray)

    if augmentation_cfg.use_gaussian_blur:
        kernel = int(0.1 * augmentation_cfg.image_size)
        kernel = ((kernel // 2) * 2) + 1
        rnd_gaussian_blur = transforms.RandomApply(
            [transforms.GaussianBlur(kernel, (0.1, 2))], p=0.1
        )
        transforms_list.append(rnd_gaussian_blur)

    transforms_list.append(transforms.ToTensor())

    if augmentation_cfg.normalize:
        normalize_transform = get_normalize(augmentation_cfg.dataset)
        transforms_list.append(normalize_transform)

    trans = transforms.Compose(transforms_list)
    return trans


def get_color_jitter(color_jitter_strength: float = 0.5) -> transforms.RandomApply:
    color_jitter = transforms.ColorJitter(
        0.8 * color_jitter_strength,
        0.8 * color_jitter_strength,
        0.8 * color_jitter_strength,
        0.2 * color_jitter_strength,
    )
    rnd_color_jitter = transforms.RandomApply([color_jitter], p=0.8)
    return rnd_color_jitter


def get_normalize(dataset: str = "cifar-10") -> transforms.Normalize:
    if dataset == "cifar-10":
        return transforms.Normalize(mean=CIFAR_10_MEAN, std=CIFAR_10_STD)
    elif dataset == "cifar-100":
        return transforms.Normalize(mean=CIFAR_100_MEAN, std=CIFAR_100_STD)
    else:
        raise NotImplementedError
